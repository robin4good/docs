---
title: Documentación
date: 2020-10-30
draft: false
weight: -10
---

{{< hint info >}}
**¡Página web y proyecto en construcción!** 
{{< /hint >}}

## ¿Qué es RobinForGood?

Es una aplicación web que ofrece apoyo vecinal al **conectar a personas o colectivos del mismo barrio que quieran formar parte de una red donde se compartan, se regalen o se reutilicen recursos**. Pretende **visibilizar a las redes de apoyo mutuo y asociaciones** que realizan este tipo de acciones **sin ánimo de lucro**. A través de la plataforma cualquier participante puede **ofrecer y solicitar un objeto o una actividad** sin ánimo de lucro. La propia web tampoco tiene ningún propósito comercial ni intención de monetizar los servicios, siendo sus dos **principales objetivos el fomento de redes solidarias y el desvinculamiento con la sociedad de consumo**.

El **público objetivo** serán las **personas que viven en situaciones de vulnerabilidad** y las **personas concienciadas sobre las desigualdades económicas y la cultura de usar y tirar** existente. En un comienzo, la zona geográfica será **Madrid**. Sin embargo desde el diseño se tendrá en cuenta la **descentralización y federación de servidores**.

Aquí se recoge toda la documentación del proyecto [RobinForGood](/docs/robinforgood/introduction). Si tienes alguna duda o quieres colaborar en el proyecto, no dudes en escribir a la dirección de correo `robinforgood@protonmail.com`. ¡Gracias por tu tiempo!

## Índice de contenidos

{{< toc-tree >}}

Esta página de documentación se ha construido con el tema de Hugo Geekdoc. Para más información, visite su [documentación](https://geekdocs.de/) y su [github](https://github.com/thegeeklab/hugo-geekdoc).
