---
title: Objetivos
date: 2020-11-16
draft: false
weight: -10
---

Los objetivos se dividen en varios grupos en función del tiempo. Se han fijado tres grupos de objetivos:

+ Primer plazo: 1 de agosto al 1 de enero 2021.
+ Segundo plazo: 2 de enero al 1 de junio 2021.
+ Tercer plazo: 2 de junio al 1 de diciembre 2021.

## Primer plazo

+ Estudio sobre el estado del arte sobre los paradigmas DevOps y DevSecOps.
+ Esbozo de unas prácticas y herramientas a utilizar de DevOps.
+ Diseño del modelo y la arquitectura del sistema completo.
+ Diseño de las medidas de seguridad y buenas prácticas.
+ Implementación de la versión funcional del código frontend y backend.
+ Despliegue mediante Kubernetes en un clúster de Raspberrys Pis.
+ Aplicación e implementación de las medidas de seguridad.

## Segundo plazo

+ Definición e integración de pruebas automatizadas.
+ Monitorización y seguimiento de incidencias de seguridad.
+ Continuar y mejorar las prácticas DevOps.
+ Redacción de una política de seguridad detallada.
+ Mejorar la gestión de entornos y secretos de los componentes de la aplicación.
+ Publicar una primera versión del desarrollo de la aplicación y extender el proyecto para crear una comunidad de desarrolladores.
+ Realización del análisis ENS y las medidas de protección legal y cumplimiento de GDPR en PILAR.

## Tercer plazo

+ Auditoría técnica de seguridad con pruebas de penetración automatizadas.
+ Utilizar pgcryto para cifrar las bases de datos Postgres.
+ Despliegue en producción.
+ Federación de servidores mediante el protocolo ActivityPub.