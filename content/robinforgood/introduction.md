---
title: Introducción
date: 2020-11-16
draft: false
weight: -10
---

## ¿Qué es RobinForGood?

Es una aplicación web que ofrece la posibilidad de **conectar a personas o colectivos que quieren donar, regalar o compartir recursos con otras personas que necesitan o quieren reutilizar** dichos recursos. 

Este proyecto pretende ser una **herramienta de conexión social a nivel local**. Es un proyecto **sin ánimo de lucro, totalmente gratuito y desarrollado con tecnologías de Software Libre y Código Abierto**. 

Actualmente, con la covid-19, han surgido muchas **redes de apoyo mutuo y despensas solidarias** para gestionar los **cuidados de personas que viven en situaciones de vulnerabilidad**. Este proyecto se plantea como una **herramienta de visibilidad y complemento organizacional a dichos colectivos**. 

## ¿Cuáles son los valores y principios éticos?

Los valores principales del proyecto son **el apoyo mutuo y cooperación para la construcción de una alternativa al modelo de consumo del capitalismo actual**. Se enfatiza en la concepción de que las personas que conforman sociedades no deberían de moverse únicamente por y para el dinero. Por lo tanto, los principios éticos derivados de las líneas del proyecto son la **solidaridad, ecologismo, transparencia y privacidad**.

## ¿Dónde se lleva a cabo?

En un principio, la zona geográfica será **Madrid**, aunque desde el diseño se tendrá en cuenta la **descentralización y federación de servidores** para que cualquier persona pueda autogestionar y autoalojar esta plataforma en otros entornos, barrios o ciudades. Al desarrollarse mediante licencias de Software Libre se aseguran las libertades de redistribuición y modificación de copias para poder adaptar el proyecto a las distintas situaciones y zonas geográficas.

## ¿Qué motivaciones llevaron a crear este proyecto?

Este proyecto surge a raíz de observar que la sociedad en la que vivimos todo se rige por la cultura de `usar y tirar`. La **sociedad de consumo** se caracteriza por la **rapidez y facilidad de consumir productos** sin mirar más allá que cubrir una  supuesta __"necesidad inmediata"__. Esta necesidad está creada por diversos factores. Uno de ellos podría ser la manipulación a través de la publicidad que influye a las personas con la finalidad de que actuen de una determinada manera y las empresas ganen un máximo beneficio económico. Los ritmos frenéticos y los múltiples estímulos a los que estamos sometidos provocan que **consumamos productos desechables o sustituibles frecuentemente**, con una **ínfima vida útil y de muy baja calidad**.

Una de las **consecuencias de la sociedad de consumo** es el **aumento de la individualidad**, intrínsecamente egoísta, que genera la **falsa creencia** de que la **acumulación de bienes es una fuente de felicidad y sinónimo de éxito personal**. Con esta mentalidad se suele tender a pensar que por cada acción que se realice tiene que existir una recompensa. Otra consecuencia es la **contaminación medioambiental** que produce este modelo no sostenible al percibir falsamente que los recursos naturales son ilimitados.

Por otra parte, el **acto de donar** suele estar vinculado a un **asistencialismo para limpiar conciencias**, donde normalmente no se suele conocer a la persona  destinataria. Además, cabe destacar que ciertas ONGs tienen problemas de transparencia con las donaciones, generando una posible confusión a los donantes sobre el destino real de las mismas.


