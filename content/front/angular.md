---
title: Angular
weight: -15
---

{{< toc >}}

## Instalación y creación de un proyecto

La instalación de `angular-cli` de forma global (-g) se realiza a través de npm<cite> [^1]:</cite> 
[^1]: [Angular Documentation, Installing Angular CLI.](https://angular.io/cli#installing-angular-cli)

``` 
npm install -g @angular/cli
```
Para crear de el nuevo proyecto se utiliza el comando:
```
ng new webapp-robin
```
Se añade un archivo de enrutado y se usa css como hoja de estilo. Aunque posteriormente se cambiará a SCSS para la utilización de Figma como generación de estilos.

La instalación de Boostrap se realiza con:
```
ng add @ng-bootstrap/ng-bootstrap --project webapp-robin
```

## Autenticación JSON Web Token (JWT)

La autenticación de los usuarios se realiza mediante JSON Web Token (JWT). Se crea una página de registro, una de inicio de sesión y otra de perfil de usuario. Tomando como referencia el tutorial de bekoder<cite> [^2],</cite> se realiza la implementación de una autenticación basada en Token. Se prefiere la autentización mediante Token a la autenticación basada en Sesiones porque si en un futuro se implementa una aplicación nativa para móvil, la autenticación basada en sesiones no será compatible<cite> [^3].</cite> El prosósito de utilizar JWT es verificar que la información se origina de una fuente autenticada. En este caso, la autenticación de cada usuario. 

[^2]: [{z}Koder, Angular 10 JWT Authentication example with Web Api.](https://bezkoder.com/angular-10-jwt-auth/)
[^3]: [{z}Koder, In-depth Introduction to JWT-JSON Web Token](https://bezkoder.com/jwt-json-web-token/)
[^4]: [Wikipedia, JSON Web Token](https://en.wikipedia.org/wiki/JSON_Web_Token)

Para la creación de un JWT, se requiere una cabecera, una carga útil o _payload_ y una firma<cite> [^4].</cite>
- En la cabecera se indica el tipo del token, en este caso es `JWT`, y el algoritmo de hash utilizado para la firma, en este caso se utulizará HMAC-SHA256.  
- En la carga útil se requieren los parámetros `userId`, `username` y `email`. Además se incluye un tiempo de expiración de 24 horas.
- En la firma se utiliza el algoritmo de hash para firmar con el secreto almacenado en el servidor los datos de la cabecera y la carga útil.

La cabecera, la carga útil y la firma se codifican en base64 y se unen formando el JWT. Para la implmentación se utilizan los paquetes npm `jsonwebtoken` y `bcryptjs` en el servidor.

> La información no se cifra con JWT, sólo se firma mediante hash, por lo que es necesario utilizar cifrado mediante SSL/TLS para habilitar HTTPS. En caso de no tener HTTPS, un atacante podría realizar un ataque de Man in The Middle (MITM) y decodificar la información.

El secreto utilizado para la firma, se almacenará de manera segura en el servidor, mientras que el Token se almacena localmente en el cliente.  

## Intercambio de recursos de origen cruzado (CORS)

Se aplica el [**intercambio de recursos de origen cruzado o CORS**](https://es.wikipedia.org/wiki/Intercambio_de_recursos_de_origen_cruzado) (Cross-origin resource sharing) como mecanismo que permite que se puedan solicitar **recursos restringidos**. CORS define una forma en la cual el navegador y el servidor pueden interactuar para determinar si es seguro permitir una petición de origen cruzado. Esto permite tener más libertad y funcionalidad que las peticiones de mismo origen, pero es adicionalmente más seguro que simplemente permitir todas las peticiones de origen cruzado<cite> [^5].</cite> 

[^5]: [Wikipedia, Cross-origin resource sharing](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)

## Crear, Leer, Actualizar y Borrar (CRUD)

Se implementan las funciones básicas de interacción con la base de datos o capa de persistencia. Concretamente para crear, leer, actualizar o borrar objetos, cuyo acrónimo es CRUD por sus siglas en inglés (Create, Read, Update and Delete). 

Para la implementación de la capa de presentación se toma como referencia el tutorial de zkoder<cite> [^6].</cite> Se implementa una página para añadir donaciones, otra para editarlas y otra para listarlas y mostrar los detalles de las donaciones. Además se crea un filtro para buscar por el título.

[^6]: [{z}Koder, Angular 10 CRUD Application example with Web API](https://bezkoder.com/angular-10-crud-app/)


## Subida de imágenes

### Subida de múltiples imágenes

La implementación de subida de múltiples imágenes se realiza tomando como referencia un tutorial de zkoder<cite> [^7].</cite> Se crea un componente donde se pueden adjuntar varias imágenes, visualizando la barra de progreso y el nombre de los archivos. Por último se crea una lista con las imágenes subidas.
[^7]: [{z}Koder, Angular 10 upload Multiple Images with Web API example](https://bezkoder.com/angular-10-upload-multiple-images/)

La creación se lleva a cabo mediante el componente descrito anteriormente y un servicio que se encargará de realizar las peticiones al servidor.

### Pruebas con ng2-file-upload

Otro método probado para la subida de imágenes fue usar el paquete NPM [`ng2-file-upload`](https://github.com/valor-software/ng2-file-upload). Se llevó a cabo tomando como referencia la [documentación oficial de ng2-dile-upload](https://valor-software.com/ng2-file-upload/)<cite> [^8]</cite> y un tutorial de [positronX.io](https://www.positronx.io/angular-8-node-express-js-file-upload-tutorial/)<cite> [^9]</cite>. También se usó `ngx-toastr` para mostrar mensajes de alerta. El módulo `ngx-toastr` depende de `@angular/animations`, por lo que se requiere su instalación. La instalación de estos paquetes se realiza mediante la siguiente instrucción:

[^8]: [Valor Software, ng2-file-upload](https://valor-software.com/ng2-file-upload/)
[^9]: [PositronX.io, Angular 11 Node & Express JS File Upload Tutorial](https://www.positronx.io/angular-8-node-express-js-file-upload-tutorial/)

```bash
npm install ng2-file-upload ngx-toastr @angular/animations --save
```

Se crea un nuevo componente llamado `upload-images` con el siguiente comando:

```bash
ng g c components/upload-images
```

En el archivo `upload-images.component.ts` se requiere incluir los nuevos componentes:

```typescript
import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ToastrService } from 'ngx-toastr';

const URL = 'http://localhost:8080/api/upload';

```

Finalmente este método se descarta por varios errores en la implementación.

## Variables de entorno

### Propias de Angular

En angular las variables de entorno se encuentran diferenciadas entre `src/environments/environment.ts` y `src/environments/environment.prod.ts`. Se utiliza por defecto el archivo de desarrollo , que es el nombrado primero. Se añaden unas variables de ejemplo:
```
export const environment = {
  production: false,
  NODE_PORT: 8080,
  MODE_ENV: "development",
  API_URL: "localhost",
  ANGULAR_URL: "localhost",
  ANGULAR_PORT: 8081,
};
```

En el archivo que se quiera usar alguna variable de entorno será necesario importar el fichero que las contiene y usarlas de la siguiente manera:
```
import { environment } from '../../environments/environment';

const port= environment.NODEPORT || 8080;
```

A pesar de que esta podría ser una opción de usar las variables de entorno, se prefiere utilizar el método del archivo `.env` como se usa en Nodejs. El método propio de Angular requiere tener las variables localmente en el interior del proyecto y podría ser una fuga de seguridad de información en caso de subirlo a un repositoio. Además de ser más útil para trabajar con varibles de entorno en Docker y Kubernetes.

### Usando .env

> Método descartado por problemas con la dependencia `dotenv` en angular.

Se instala la dependencia npm `dotenv` con el siguiente comando:
```
npm install -s dotenv
```
Para leer el archivo `.env` se requiere importar el paquete `dotenv` y ejecutar la función `config()` en el ficjero `src/environments/environment.ts`. Mientras que para acceder a las variables desde el código se utiliza ``${process.env.API_URL}`` o ``${process.env.PORT}``. 

```
require('dotenv').config();

export const environment = {
  production: false,
  NODE_PORT: `${process.env.API_URL}`,
  MODE_ENV: `${process.env.MODE_ENV}`,
  API_URL: `${process.env.API_URL}`,
  ANGULAR_URL: `${process.env.ANGULAR_URL}`,
  ANGULAR_PORT: `${process.env.ANGULAR_PORT}`,
};

```

En caso de querer realizar un pequeño script para las variables de entorno usadas en `prod` se seguirá la siguiente referencia<cite> [^10]</cite>.
[^10]: [Ninad Subba, Setup dotenv to Access Environment Variables in Angular](https://medium.com/javascript-in-plain-english/setup-dotenv-to-access-environment-variables-in-angular-9-f06c6ffb86c0)


{{< hint danger >}}
**IMPORTANTE: En caso de querer cambiar el puerto 8081, que es donde se sirve la aplicación, se debe cambiar directamente desde el archivo package.json** 
{{< /hint >}}
