---
title: Git
weight: -15
---

## Configurar git 
Configuración global de git con un **nombre de usuario o un correo electrónico**:
```bash
git config --global user.name "Nombre de Ejemplo"
git config --global user.email nombre@ejemplo.com 
```
Para listar la configuración global:
```
git config --global --list
```

## Clonar un repositorio
Si es un repositorio **local**:
```bash
git clone /ruta/del/repositorio
```

En caso de querer utilizar un proyecto en un repositorio **remoto** mediante https:
```bash
git clone https://<URL_repositorio_remoto>
```

## Iniciar un repositorio
Estando en el directorio en el que queremos **iniciar el repositorio** introducimos en el siguiente comando:
```bash
git init
```

## Crear un commit
Primero se **agregan los archivos modificados al index** y se **confirma el commit** con un mensaje explicativo:
```bash
git add .
git commit -m "Mensaje explicativo de que se ha hecho"
```

## Ver la lista de archivos que han cambiado o están en el index
```bash
git status
```

## Crear una rama o cambiar a otra ya creada
Si quieres **crear** una rama:
```bash
git checkout -b <nombre_rama>
```
Para cambiar a una rama ya creada:
```bash
git checkout <nombre_rama>
```

## Conectar un repositorio remoto
Primero, si quieres **ver los repositorios ya conectados**:
```bash
git remote -v
```
Para **añadir un nuevo repositorio remoto ya existente**:
```bash
git remote add origin https://<URL_repositorio_remoto>
``` 

## Enviar los cambios desde una rama local al repositorio remoto

```bash
git push origin <nombre_rama>
```

## Buscar los cambios realizados desde el repositorio remoto que no están en el repositorio local

```bash
git fetch <origin>
```

## Fusionar o unir los cambios realizados en la rama “nombre_rama” con la rama actual
```bash
git merge <nombre_rama>
```

## Fusionar los cambios realizados en el repositorio local con e remoto (unifica los comandos fetch y merge en un único comando)
```bash
git pull 
```

## Visualizar los conflictos presentes
Si se quieren todos los conflictos:
```bash
git diff 
``` 
Si se quiere **entre dos ramas**:
```bash
git diff <rama_origen> <rama_objetivo>
``` 
## Visualizar todas las ramas del repositorio local y remoto
```bash
git branch -a -v
```

## Borrar una rama 
```bash
git branch -d <nombre_rama>
```

## Borrar archivos del index y del directorio que está trabajando
```bash
git rm <nombre_archivo>
```

## Visualizar la historia de commits de la rama donde te encuentre
```bash
git log --graph
```

## Visualizar la historia de commits de todo (repositorio local y remoto) 
```bash
git log --graph --all
```

## Inspeccionar un repositorio 
Si se quiere inspccionar un repositorio **local**:
```bash
git  show
```

Si se trata de un repositorio **remoto**: 
```bash
git remote show origin
```


## Ignorar archivos

Comando para ignorar el directorio `node_modules` y para no subirlo al repositorio git:
```bash
touch .gitignore && echo "node_modules/" >> .gitignore && git rm -r --cached node_modules ; git status
```
