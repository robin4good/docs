---
title: Monitorización
date: 2021-01-16
draft: false
weight: 40
---

{{< toc >}}

## Monitorización de CPU y memoria a través de metrics-server

Para realizar un escalado horizontal en función de la CPU y memoria en uso de los nodos, se utiliza el servidor de métricas de Kubernetes. Este servidor ajusta automáticamente los recursos necesarios para los servicios desplegados en contenedores.

Para visualizar los datos de CPU y memoria de los nodos a través del CLI `kubectl top` ejecutar:
```bash
kubectl top nodes
```
Mientras que para visualizar los datos de CPU y memoria de los pods ejecutar:
```bash
kubectl top pods -A
``` 

## Prometheus

Se instala Prometheus a través de un chart de helm mantenido por la comunidad de la herramienta.

## Grafana

Se instala grafana a través de arkade. Se muestra un dashboard del cluster de kubernetes con k3s.

## Elasticsearch + Kibana + Fluent

[Logging in Kubernetes with Elasticsearch, Kibana, and Fluentd](https://mherman.org/blog/logging-in-kubernetes-with-elasticsearch-Kibana-fluentd/)
