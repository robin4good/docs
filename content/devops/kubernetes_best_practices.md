---
title: Buenas prácticas en Kubernetes
date: 2021-01-13
draft: false
weight: 20
---

{{< toc >}}

## Actualización de imágenes

Para mantener las imágenes actualizadas a la versión con la etiqueta `latest`, asignar a [imagePullPolicy](https://kubernetes.io/docs/concepts/containers/images/#updating-images) el valor `Always`.


## Limitar el uso de recursos 

En los despliegues se recomienda limitar el uso 

```yaml
limits:
    memory: 512Mi
    cpu: "1"
requests:
    memory: 256Mi
    cpu: "0.2"  
```

## Crear un alias del comando kubectl

Para crear un alias del comando kubectl se utiliza el siguiente comando:
```bash
alias k8='kubectl'
```
En caso de ser en k3s se utiliza:
```bash
alias k='sudo k3s kubectl'
```

Para crearlo definitivamente y poder usarlo en cada reinicio, se modifica el archivo `~/.bashrc`:
```bash
nano ~/.bashrc
```
Y se añade:
```
#Alias k3s y k8s
alias k='sudo k3s kubectl'
alias k8='kubectl'
```
Para hacerlo efectivo en dicha sesión se utiliza:
```bash
source ~/.bashrc
```

## Cambio del namespace por defecto

Primero se debe crear un namespace es con el siguiente comando:
```bash
kubectl create namespace nuevo_namespace
```

Para cambiar el namespace por defecto (`default`) por otro nombre más adecuado se utiliza:
```
kubectl config set-context --current --namespace=nuevo_namespace
```

## Ver logs de un pod

Por ejemplo los logs que arroja un pod de Postgres:
```
k logs postgres-0 --namespace develop
```

## Variables de entorno

Utilizar variables de entorno en secretos.