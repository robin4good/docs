---
title: Docker-compose
date: 2021-01-19
draft: false
weight: -10
---

{{< toc >}}

## Instalación

+ Distribuciones basadas en Arch:
```
sudo pacman -S docker-compose
```

+ Distribuciones basadas en Debian:
```
sudo apt install -y docker-compose
```

Se comprueba la versión con:
```
docker-compose -v
```

## Creación del fichero docker-compose.yml

Se crea un archivo docker-compose.yml con el siguiente contenido:

```
version: "3.8"
services:
  db_postgres:
    image: postgres
    restart: always
    environment:
        POSTGRES_USER: user
        POSTGRES_PASSWORD: password
        POSTGRES_DB: db_name
    volumes: 
      - ./tmp/pgdata:/var/lib/postgresql/data
    ports:
      - "5432:5432"
  backend:
    image: robinforgood/backend:latest
    depends_on:
      - db_postgres
    environment:
      DATABASE_URL: postgres://user:password@db_postgres:5432/db_name
    ports:
      - "8080:8080"
  frontend:
    image: robinforgood/webapp:latest
    ports:
      - "8081:80"
```

Los servicios a desplegar son los siguientes:

+ **db_postgres**: una base de datos postgres, con las variables de entorno seleccionadas en el ORM, en este caso se utiliza Sequelize, un volumen externo almacenado en la carpeta `/tmp/pgdata` desde la ejecución y el puerto `5432` para exponer el contenedor.
+ **backend**: la imagen del servidor node.js con la variable de entorno para conectarse a la base de datos, exponiendo el servicio por el puerto `8080`. Es accesible a través de la dirección `http://localhost:8080`.
+ **frontend**: la imagen de la capa de presentación con Angular exponiendo el servicio por el puerto `8081`. Es accesible a través de la dirección `http://localhost:8081`.

Para solucionar el error de `connect ECONNREFUSED 127.0.0.1:5432` entre Sequelize y Postgres, es necesario comprender la conexión entre aplicaciones. Al correr aplicaciones usando Docker en un mismo host, es necesario cambiar la dirección host `127.0.0.1` o `localhost` por la dirección IP del contenedor de la base de datos. Esto se cambia en el archivo docker-compose y en el archivo db.config.js del servidor backend<cite> [^1].</cite> Además es necesario añadir un temporizador de 5 segundos al servidor nodejs para que se espere a que la base de daos postgres se haya configurado correctamente<cite> [^2].</cite>
[^1]: [Stack Overflow, ECONNREFUSED for Postgres on nodeJS with dockers](https://stackoverflow.com/questions/33357567/econnrefused-for-postgres-on-nodejs-with-dockers)

[^2]: [ Ben Awad, Docker Compose Tutorial with PostgreSQL and Node.js](https://www.youtube.com/watch?v=A9bA5HpOk30)

Posteriormente se cambian los parámetros de la base de datos por variables de entorno y queda este `docker-compose.yml`:

```
version: "3.8"
services:
  db_postgres:
    image: postgres
    restart: always
    environment:
        POSTGRES_USER: ${POSTGRES_USER}
        POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
        POSTGRES_DB: ${POSTGRES_DB}
        POSTGRES_HOST: ${POSTGRES_HOST}
    volumes: 
      - ./tmp/pgdata:/var/lib/postgresql/data
    ports:
      - "5432:5432"
  backend:
    image: robinforgood/backend:${DEV_VERSION_BACK}
    depends_on:
      - db_postgres
    environment:
      DATABASE_URL: ${POSTGRES_DIALECT}://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:5432/${POSTGRES_DB}
    ports:
      - "8080:8080"
  frontend:
    image: robinforgood/webapp:${DEV_VERSION_FRONT}
    ports:
      - "8081:80"
```

## Ejecución

Una vez creado el archivo docker-compose.yml y desde el mismo directorio se ejecuta:
```
docker-compose up --build
```
El flag `--build` indica que se muestren los logs durante la construcción de los contenedores. 

Para inicializar la base de datos, se crea el fichero `docker-entrypoint-initdb.d/init.sql` con las órdenes SQL necesarias para ejecutar en el inicio.

En caso de querer volver a levantar los contenedores con una nueva configuración inicial se obtendrá el aviso `PostgreSQL Database directory appears to contain a database; Skipping initialization`. Para forzar la inicialización será necesario eliminar el volumen creado en la carpeta de `robin_backend` con el directorio `/tmp/pgdata`.

## Detener los servicios

Para detener los servicios utilizar:
```bash
docker-compose down
```

En caso de obtener un error donde se indique el puerto `5432` está en uso. Obtener el ID del proceso (pid) con:
```
sudo ss -lptn 'sport = :5432'
```
Eliminar el proceso con `sudo kill -9 <pid>`.

En caso de obtener el error `ERROR: for backend  Cannot start service backend...Bind for 0.0.0.0:8080 failed: port is already allocated`:

Listar, parar y elimanr el contenedores que esté usando ese puerto.
```bash
docker container ls
docker container stop <container-ID>
docker container rm <container-ID>
```

## Convertir docker-compose.yml a kubernetes

Para la conversión, se utiliza la herramienta `kompose`<cite> [^3].</cite>
[^3]: [Kubernetes Documentation, Translate a Docker Compose File to Kubernetes Resources](https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/)

Siguiendo la documentación de la herramienta se instala mediante:

```
curl -L https://github.com/kubernetes/kompose/releases/download/v1.21.0/kompose-linux-amd64 -o kompose
chmod +x kompose
sudo mv ./kompose /usr/local/bin/kompose
```

Una vez instado se ejecuta `kompose convert` desde el mismo directorio que el archivo docker-compose.yml. 

Al realizar el docker-compose con variables de entorno, se requiere añadir manualmente las variables a los archivos de los despliegues.


