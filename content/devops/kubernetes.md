---
title: Despliegue de una aplicación en Kubernetes
date: 2021-13-01
draft: false
weight: 10
---

{{< toc >}}

## Copiar los archivos YAML al nodo master

Para copiar los archivos a través de SSH de se utiliza en comando `scp`. En caso de copiar los archivos recursivamente de un directorio utilizar el flag `-R`. Para utilizar un puerto concreto el flag `-p`. La estructura es la siguiente:
```bash
scp -r /local/directory remote_username@10.10.0.2:/remote/directory
```

## Creación de un namespace

Los `namespaces` o espacio de nombres permiten establecer un nivel adicional de separación entre los contenedores y pods que comparten un mismo clúster. También facilita la creación de cuotas para limitar los recursos disponibles para cada `namespace`. Se pueden considerar como "clústers virtuales" sobre un clúster físico. Por defecto Kubernetes crea el namespace `kube-system`, para el propio sistema, y `default`, para los nuevos objetos<cite> [^1].</cite> Para ver los namespaces creados ejecutar
[^1]: [On the Dock, Espacios de nombres en Kubernetes](https://onthedock.github.io/post/170723-espacios-de-nombres-en-k8s/)

```bash
kubectl get namespaces
```
La forma más sencilla de crear un namespace es con el siguiente comando:
```bash
kubectl create namespace nombre-de-ejemplo
```

También se puede crear un nuevo namespace desde un archivo. Para ello se crea un archivo `YAML` llamado `ns-develop.yaml`:
```yaml
apiVersion: v1
kind: Namespace
metadata:
   name: develop
```
Para activarlo, ejecutar:
```bash
kubectl create -f ns-develop.yaml
```
Para eliminarlo, ejecutar:
```bash
kubectl delete ns develop
```
Para ejecutar un objeto con un namespace determinado:
```
kubectl apply -f object.yaml --namespace=develop
```

## Cambio del namespace por defecto

Otro método más recomendado es cambiar el namespace por defecto (`default`) por otro nombre más adecuado.
```
kubectl config set-context --current --namespace=nuevo_namespace
```

## Despliegue básico de una aplicación sin estado

## Creación de un despliegue 

Se crean los `deployment` de la capa cliente y de la capa servidor, en el namespace `develop`, con los siguientes comandos:

```
kubectl create deployment frontend --image=robinforgood/webapp:latest --namespace=develop
kubectl create deployment backend --image=robinforgood/backend:latest --namespace=develop
```

### Creación de los servicios

Para exponer los servicios, en el namespace `develop`, se utilizan los siguientes comandos:
```
kubectl create service nodeport frontend --tcp=80:8081 --namespace=develop
kubectl create service nodeport backend --tcp=8080:8080 --namespace=develop
```

### Conocer el puerto de despliegue

Para conocer el puerto de despliegue se describe el servicio de la capa de presentación. Una vez conocido el puerto y el nodo worker donde se ha desplegado, se accede a la aplicación a través de `http://<IP-nodo>:<puerto>`.

```
kubectl describe service frontend --namespace=develop
```

### Eliminación de un despliegue

Para eliminar un despliegue llamdo `nombre-despliegue` ejecutar:

```bash
kubectl delete deployments nombre-despliegue
```

### Creación de un secreto para acceder a un contenedor privado

Se crea un secreto llamado `regcred` mediante el comando<cite> [^2]:</cite> 
[^2]: [Kubernetes Documentation, Create a Secret by providing credentials on the command line](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#create-a-secret-by-providing-credentials-on-the-command-line)

```bash
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>
```
Donde las opciones son:
- <your-registry-server> el FQDN del resgristro privado de Docker (https://index.docker.io/v1/ para DockerHub).
- <your-name> usuario de Docker.
- <your-pword> contraseña de Docker.
- <your-email> el correo electrónico de Docker.

Para inspeccionar dicho secreto utilizar:
```bash
kubectl get secret regcred --output=yaml
```
Creación de un Pod que use el secreto ` regcred`:
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: private-reg
spec:
  containers:
  - name: private-reg-container
    image: <your-private-image>
  imagePullSecrets:
  - name: regcred
```

## Despliegue de una aplicación con estado

### Despliegue de Postgres manualmente

#### Config Map
Lo primero es desplegar un objeto de tipo `ConfigMap` donde se guarden las variables de entorno para inicializar la base de datos. Se guarda lo siguiente en un fichero llamado `postgres-statefulset.yaml`.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: postgres-config
  labels:
    app: robinforgood
    tier: postgres
data:
  POSTGRES_DB: database_name
  POSTGRES_USER: username
  POSTGRES_PASSWORD: secrectpassword
  PGDATA: /var/lib/postgresql/data/pgdata
```

Se despliega con el comando:
```
kubectl apply -f postgres-configmap.yaml
```

#### StatefulSet
Para desplegar un deployment conocido como `StatefulSet`, específico para mantener el estado de la base de datos, se crea un archivo `postgres-statefulset.yaml` con el siguiente contenido:

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres
spec:
  serviceName: "postgres"
  replicas: 1
  selector:
    matchLabels:
        app: robinforgood
        tier: postgres
  template:
    metadata:
      labels:
        app: robinforgood
        tier: postgres
    spec:
      containers:
      - name: postgres
        image: postgres:13-alpine
        envFrom:
          - configMapRef:
              name: postgres-config
        ports:
        - containerPort: 5432
          name: postgresdb
        volumeMounts:
        - name: postgresdb
          mountPath: /var/lib/postgresql
          subPath: postgres
  volumeClaimTemplates:
  - metadata:
      name: postgresdb
    spec:
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: 1Gi
```

Se despliega con el comando:
```bash
kubectl apply -f postgres-statefulset.yaml
```

Para ver los logs del contenedor se utiliza:
```bash
kubectl logs postgres-0 --namespace develop
```

Para abrir una consola dentro del contendor se usa:
```bash
kubectl exec -it postgres-0 --namespace develop -- bash
```

Se inicia sesión con el usuario postgres con:
```bash
su postgres -l
```

Se crea una consola con la base de datos `database_name`:
```bash
psql -h localhost -U user database_name
```

Se puede acceder y ver que en un inicio está vacía. Por lo tanto, se concluye con que se ha creado correctamente la base de datos.


#### Service

Se despliga el servicio asociado al deployment/statefulset mediante el siguiente fichero `postgres-service.yaml`:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: postgres
  labels:
    app: robinforgood
    tier: postgres
spec:
  type: NodePort
  ports:
   - port: 5432
     name: postgres
  selector:
    app: robinforgood
    tier: postgres
```

Se aplica el servicio mediante el comando:
```bash
kubectl apply -f postgres-service.yaml
```

### Despliegue del backend

#### Deployment
Se crea un archivo llamado `backend-deployment.yaml` con el siguiente contenido:

> Nota: la dirección IP del host es la del servicio de postgres. Se obtiene con el comando `kubectl describe svc postgres`. Teóricamente debería de ser el nombre del servicio aplicado al statefulset de postgres pero no se resuelve por el DNS. Además, se añade la IP y el puerto del pod de frontend como variable de entorno para que se resuelva la política de CORS correctamente.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: robinforgood
    tier: backend
  name: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: robinforgood
      tier: backend
  template:
    metadata:
      labels:
        app: robinforgood
        tier: backend
    spec:
      containers:
      - env:
        - name: POSTGRES_HOST
          value: 10.43.158.59
        - name: POSTGRES_DB
          value: database_name
        - name: POSTGRES_PASSWORD
          value: secrectpassword
        - name: POSTGRES_USER
          value: username
        - name: POSTGRES_PORT
          value: "5432"
        image: robinforgood/backend:0.1.7
        imagePullPolicy: Always
        name: backend
        resources:
          requests:
            memory: "128Mi"
            cpu: "250m"
          limits:
            memory: "256Mi"
            cpu: "500m"
        ports:
        - containerPort: 8080
      restartPolicy: Always
```
Se aplica el despliegue mediante el comando:
```bash
kubectl apply -f backend-deployment.yaml
```

#### Service

Se crea el archivo `backend-service.yaml` con el servicio del deployment del backend de Nodejs.

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: robinforgood
    tier: backend
  name: backend
spec:
  type: NodePort
  ports:
  - name: "8080"
    port: 8080
    targetPort: 8080
  selector:
    app: robinforgood
    tier: backend
```
Se aplica el servicio mediante el comando:
```bash
kubectl apply -f backend-service.yaml
```

### Despliegue del frontend

#### Deployment
Se crea un archivo llamado `frontend-deployment.yaml` con el siguiente contenido:

> Nota: se indica la IP del nodo y el puerto que da acceso al pod de backend para que se resuelva correctamente la política de CORS.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
    name: frontend
spec:
    replicas: 1
    selector:
        matchLabels:
            app: robinforgood
            tier: frontend
    template:
        metadata:
            labels:
                app: robinforgood
                tier: frontend
        spec:
          containers:
          - name: webapp
            image: robinforgood/webapp:0.1.7
            imagePullPolicy: Always
            resources:
              requests:
                memory: "128Mi"
                cpu: "250m"
              limits:
                memory: "256Mi"
                cpu: "500m"
            ports:
            - name: http
              containerPort: 80
            env:
            - name: API_URL
              value: 192.168.1.78
            - name: NODE_PORT
              value: "31408"
```

Se aplica el deployment mediante el comando:
```bash
kubectl apply -f frontend-deployment.yaml
```

#### Service

Se crea el servicio del frontend en el archivo `frontend-service.yaml`:

```yaml
apiVersion: v1
kind: Service
metadata:
    name: frontend
spec:
    type: NodePort
    ports:
    - protocol: TCP
      port: 8081
      targetPort: 80
    selector:
        app: robinforgood
        tier: frontend

```

Se aplica el servicio mediante el comando:
```bash
kubectl apply -f frontend-service.yaml
```


### Despliegue de Postgres mediante KubeSails

Primero se requiere acceder a la plataforma [Kubesail](https://kubesail.com) y configurar el cluster. Una vez configurado se escoge la plantilla de Postgres y se instalan los componentes.

Para crear una consola dentro del pod de Postgres se utiliza el comando:

```bash
kubectl exec -it <nombre-pod-postgres>  -- bash
```

Para acceder a las bases de datos se utiliza:

```bash
psql -h localhost -U postgres
\l
```

Se crea la base de datos con `database_name`, se crea un usuario `username` con una contraseña `password` y se dan todos los privilegios sobre la base de datos `database_name`.

```
CREATE DATABASE database_name;
CREATE USER username WITH PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE database_name to username;
```

## Recursos adicionales

a) [Kubeapps](https://kubeapps.com/).
b) [Kube apps no sporta la arquitectura ARM debido a las imágenes de docker de Bitname](https://github.com/kubeapps/kubeapps/issues/929).
c) [Crunchy Container Suite tampoco soporta la arquitectura ARM](https://access.crunchydata.com/documentation/crunchy-postgres-containers/latest/).
d) [Arkade](https://github.com/alexellis/arkade).
e) [Openfaas](https://www.openfaas.com/).