---
title: Configuración de GitLab
date: 2021-01-13
draft: false
weight: -15
---

{{< toc >}}

## Configuración de clave SSH ED25519

Para realizar la autenticación mediante uso de claves público privadas y evitar introducir y enviar la contraseña en cada `push` o `clone`, se recomienda utilizar SSH. Para ello se utilizará SSH ED25519 por ser la más recomendable que RSA, al utilizar curvas elíticas en el cifrado y requerir menos potencia para obtener conexiones más rápidas y seguras. Se genera una clave SSH ED25519 con los siguientes parámetros:

```
ssh-keygen -o -a 100 -t ed25519 -f $HOME/.ssh/id_ed25519 -C "robinforgood@protonmail.com"
```
Los flags utilizados son:
- -o: Guarda la clave privada usando el nuevo formato OpenSSH en lugar del formato PEM.
- -a: Son los números de las rondas de KDF (Key Derivation Function). Cuanto mayor sea el número la verificación será más lenta y mayor protección contra fuerza bruta en caso de que la clave privada sea robada.
- -t: Especifica el tipo de clave a generar, en este caso el Ed25519.
- -f: Especifica el nombre del archivo de la clave generada.
- -C: Especificar un comentario informativo. Habitualmente tiene el formato `<login>@<hostname>`.

Se añade la clave desde la web en `Settings>SSH Keys`. 

Una vez añadida la clave, se tiene que configurar SSH para no utilizar el par de claves por defecto con los siguientes comandos:

```
eval $(ssh-agent -s)
ssh-add $HOME/.ssh/id_ed25519
```

## Añadir o eliminar repositorio remoto

Y se añade el remositorio remoto con:

```
git remote add origin git@gitlab.com:robinforgood/docs.git
```
En caso de tener un repositorio remoto ya creado, será necesario eliminarlo previamente con:
```
git remote rm origin
```

## Despliegue de páginas estáticas en GitLab

Al utilizar el generador estático [Hugo](https://gohugo.io/) para esta página de documentación, se realiza el CI/CD mediante [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). El despliegue se consigue mediante un archivo [.gitlab-ci.yml](https://gitlab.com/robinforgood/docs/-/blob/master/.gitlab-ci.yml) y se configura el subdominio de la rama `develop`. 
