---
title: Ansible
date: 2021-01-15
draft: false
weight: 35
---

{{< toc >}}

## Instalación de Ansible

- Distribuciones basadas en Debian:
```
sudo apt install -y ansible
sudo apt install -y software-properties-common
```

- Distribuciones basadas en Arch:
```
sudo pacman -Sy ansible
sudo pacman -Sy software-properties-common
```

Se comprueba la instalación y la localización de archivos de configuración por defecto en:
```
ansible --version
```

## Configuración del entorno

### Mapeo de IP a nombres
Desde la máquina utilizada para configurar las raspberrys pi, se añade a `/etc/hosts` lo siguiente: 
```
192.168.1.150 master.node master m
192.168.1.151 worker.node1 worker1 w1
192.168.1.152 worker.node1 worker2 w2
```

### Configuración básica

Por defecto el directorio de Ansible es `/etc/ansible/` y se puede trabajar directamente desde ahí. Aunque es recomendable copiar los archivos de dicho directorio a un directorio local en caso de querer trabajar con varias máquinas.

```
cp -R /etc/ansible/ ansible-raspberry-pis
```
Se configura el archivo `ansible.cfg` con:
```
[defaults]
inventory       = inventory.yaml
```
Se crea el fichero `inventory.yaml` donde se almacenan los hosts a configurar y se especifica que se utilice python3 en vez de python:
```
---
all:
  hosts:
    master:
      ansible_host: master
    worker1:
      ansible_host: worker1
    worker2:
      ansible_host: worker2
  vars:
    ansible_python_interpreter: /usr/bin/python3
```

Para probar la conexión ejecutar:
```
ansible -m ping all
```
En un inicio, recomienda utilizar usuarios en las máquinas que no requieran contraseña para sudo. Aunque también se puede solicitar la contraseña sudo utilizando el módulo user. A continuación se expone un ejemplo:
```
ansible -b -K user -a 'name=testuser' all
```

## Ansible ad-hoc

Para ejecutar un comando específico se utiliza lo siguiente:
```
ansible -m shell -a 'echo Hola!' all
```

## Ansible playbooks

Para la creación de un playbook, es necesario crear la estructura `roles/basic/tasks` con un archivo `main.yaml` en su interior que tenga las tareas que se quieren automatizar. Por ejemplo, para automatizar las actualizaciones de paquetes y del sistema en un sistema operativo basado en Debian, puede tener la siguiente estructura:
```
---
- name: Update apt-get repo and cache
  apt: update_cache=yes force_apt_get=yes cache_valid_time=3600
- name: Upgrade all apt packages
  apt: upgrade=dist force_apt_get=yes
- name: Check if a reboot is needed for HypriotOS boxes
  register: reboot_required_file
  stat: path=/var/run/reboot-required get_md5=no
- name: Reboot the servers
  reboot:
    msg: "Reboot initiated by Ansible due to kernel updates"
    connect_timeout: 5
    reboot_timeout: 300
    pre_reboot_delay: 0
    post_reboot_delay: 30
    test_command: uptime
  when: reboot_required_file.stat.exists
```

Además, se crea el archivo `playbook.yaml` con la siguiente estructura:

```yaml
---
- hosts: all
  become: true
  roles:
    - basic
```

Para ejecutar el playbook, se utiliza el comando:
```bash
ansible-playbook playbook.yaml
```
