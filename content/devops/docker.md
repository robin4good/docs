---
title: Instalación de Docker
date: 2021-01-03
draft: false
weight: -13
---

{{< toc >}}

Lo primero de todo es comprobar si Docker está instalado mediante el comando:
```bash
docker version
``` 

## Ejecutar el demonio Docker
Para comprobar que se está ejecutando el demonio Docker, ejecutar alguno de los siguientes comandos:
```bash
docker ps
docker info
```
Devolverá el siguiente mensaje de error:
`Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?`

Para levantar el demonio se utiliza:
```bash
sudo systemctl start docker.service
sudo systemctl enable docker.service
```

Para incluir al usuario actual en el grupo docker, ejecutar:
```bash
sudo usermod -aG docker $USER
reboot
```
## Detener el demonio Docker

Si se quiere parar el demonio Docker, ejecutar:
```bash
sudo systemctl stop docker
```

## Instalación de Docker Rootless

Hasta la versión `20.10`, se requería ejecutar el demonio Docker con un usuario con privilegios root. Esto es una mala práctica de seguridad porque si un atacante consiguiese escapar del aislamiento del contenedor, tendría directamente los privilegios root.

El modo sin permisos root, también conocido como _rootless_, ejecuta el demonio Docker y los contenedores en un espacio de nombres de usuario. No utiliza binarios con bits SETUID o las _capabilities_ de linux, a excepción de newuidmap y newgidmap, que son necesarios para permitir el uso de múltiples UIDs/GIDs en el espacio de nombres de usuario<cite> [^1].</cite>
[^1]: [Docker Documentation. Run the Docker daemon as a non-root user (Rootless mode)](https://docs.docker.com/engine/security/rootless/)

La instalación se realiza siguiendo la documentación oficial de Docker sobre el [modo rootless](https://docs.docker.com/engine/security/rootless/). 


