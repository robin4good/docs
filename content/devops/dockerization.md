---
title: Dockerización de aplicaciones
date: 2021-01-09
draft: false
weight: -11
---

{{< toc >}}

## Creación del fichero Dockerfile

La creación del contenedor docker de la aplicación Angular se lleva realiza como multietapa. En la primera etapa se construye el código en una imagen ligera de node (`alpine`), donde se seleciona el directorio de trabajo, se copian los archivos `package.json` y `package-lock.json`, se cambia a un usuario no root, se instalan las dependencias, se copian los archivos con los permisos necesarios y se construye la aplicación Angular con los flags de producción. En la segunda etapa se coge la imagen de un servidor `nginx` y se copian los archivos de la aplicación para exponerla a través del puerto `8081`<cite> [^1].</cite>
[^1]: [Digital Ocean Community, How To Build a Node.js Application with Docker](https://www.digitalocean.com/community/tutorials/how-to-build-a-node-js-application-with-docker)


``` docker
### STAGE 1: Build ###
FROM node:10.23.1-alpine3.9 As builder
# Creation of app directory
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
# Installation of app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# User Non-root
USER node

# To build the code for production:
#RUN npm ci --only=production
RUN npm install

COPY --chown=node:node . .

RUN npm run build --prod

### STAGE 2: Run ###
FROM nginx:1.19.6-alpine
COPY --from=builder /home/node/app/dist/webapp-robin/ /usr/share/nginx/html
EXPOSE 80/tcp
``` 

## Construcción del contenedor

Se contruye la imagen docker con el siguiente comando:
```
docker build -t robinforgood/webapp:0.0.1 .
```
Se comprueba que se ha construido con:
```
docker images
```

### Construcción de una imagen con multiarquitectura

En caso de que la arquitectura de la CPU de la máquina dónde se quiera construir la imagen no soporte todas las arquitecturas, será necesario realizarlo a través de la herramienta `buildx` y el emulador de arquitecturas QEMU. La forma más sencilla de usar QEMU es mediante un contenedor con los siguientes comandos<cite> [^2]:</cite>
[^2]: [Stack Overflow, Docker - Cannot build multi-platform images with docker buildx](https://stackoverflow.com/questions/60080264/docker-cannot-build-multi-platform-images-with-docker-buildx?answertab=votes#tab-top) 

```
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --name builder --driver docker-container --use
docker buildx inspect --bootstrap
```
La salida del último comando mostrará todas las arquitecturas disponibles. Para construir la imagen ejecutar:
```
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 --push -t robinforgood/webapp:0.0.4 .
```

## Ejecución del contenedor

La ejecución de un contenedor se realiza con el siguiente comando:

```
docker run --name webapp -p 80:80 -d robinforgood/webapp:0.0.1
```
Los flags indican:
- --name: nombre del contendor
- -p puerto_externo:puerto_interno del contendor
- -d: modo en primer plano 

### PostgreSQL

La ejecución de un contenedor Postgres con las variables de entorno y volumen de ejemplo se realiza mediante el comando<cite> [^3]:</cite>
[^3]: [Docker Hub, postgres](https://registry.hub.docker.com/_/postgres/) 

```
docker run -d \
    --name some-postgres \
    -e POSTGRES_PASSWORD=mysecretpassword \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /custom/mount:/var/lib/postgresql/data \
    postgres
```

## Subir la imagen a Docker Hub
Para subir la imagen al repositorio de DockerHub, se necesita crear una cuenta e iniciar sesión a través de:
```bash
docker login -u nombre_usuario_dockerhub
```
Pedirá la contraseña de la cuenta y cuando se ingrese, se creará un archivo `~/.docker/config.json` en el directorio principal del usuario con las credenciales de Docker Hub.

Para añadir una etiqueta a la imagen objetivo que se quiere subir, escribir en este formato `docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]`<cite> [^4]:</cite>
[^4]: [Docker Documentation. docker tag](https://docs.docker.com/engine/reference/commandline/tag/)

```
docker tag imagen_local:etiqueta imagen_objetivo:etiqueta
```
Un ejemplo sería:
```
docker image tag robinforgood/backend:0.0.3 robinforgood/backend:latest
```

Entonces ya se puede subir la imagen al repositorio con la etiqueta añadida anteriormente:
```
docker push nombre_usuario_dockerhub/nombre-imagen
```
Por ejemplo:
```
docker push robinforgood/backend:latest
```
A partir de ese momento, se puede ejecutar una imagen directamente de Docker Hub con el siguiente comando:
```
docker run --name nombre-imagen -p 80:8080 -d nombre_usuario_dockerhub/nombre-imagen
```
