---
title: Node.js
draft: false
weight: -20
---

{{< toc >}}

## Instalación

Instalación de node y npm:

* Distribuciones basadas en Arch:
```bash
sudo pacman -Sy nodejs npm
```
* Distrubuciones basadas en Debian:
```bash
sudo apt install -y nodejs npm
```

Creación de una carpeta para el proyecto:
```bash
mkdir robin-backend
cd robin-backend
```
Inicialización la aplicación node con:
```bash
npm init 
```
Instalación de los módulos:
* **express**: es el framework de nodejs para la implementación de API RESTs.
* **sequelize**: es un ORM de Node.js basado en promesas para Postgres, MySQL, MariaDB, SQLite y Microsoft SQL Server.
* **pg**: es la base de datos PostgreSQL.
* **pg-hstore**: sirve para convertir los datos en el formato hstore de PostgreSQL.
* **body-parser**: es el middleware de análisis del cuerpo de Node.js. Es responsable de analizar los cuerpos de las solicitudes entrantes en un middleware antes de manejarlo.
* **cors**: CORS (Cross-Origin Resource Sharing) es un mecanismo que permite que se puedan solicitar recursos restringidos (como por ejemplo, las tipografías) en una página web desde un dominio diferente del dominio que sirvió el primer recurso<cite> [^1].</cite>
[^1]: [Wikipedia, Intercambio de recursos de origen cruzado](https://es.wikipedia.org/wiki/Intercambio_de_recursos_de_origen_cruzado)
```bash
npm install express sequelize pg pg-hstore body-parser cors --save
```

## Configuración de PostgreSQL y Sequelize

Se crea el directorio `config` con el archivo `db.config.js` el cual tiene en su interior:
```
module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "123",
  DB: "testdb",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
``` 

Los primeros cinco parámetros son para la conexión PostgreSQL. `pool` es opcional, se utilizará para la configuración de conexiones Sequelize:
* `max`: número máximo de conexiones en pool  
* `min`: número mínimo de conexiones en pool
* `idle`: tiempo máximo, en milisegundos, que una conexión puede estar inactiva antes de ser liberada
* `acquire`: tiempo máximo, en milisegundos, que el grupo intentará conectarse antes de lanzar el error

Más información en la [referencia de la API del constructor de Sequelize](https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor)<cite> [^2]</cite>.

[^2]: [Sequelize Documentation, Public Constructors](https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor)

## JSON Web Token (JWT) Authentication

La autenticación de los usuarios se realiza mediante JSON Web Token (JWT). Se crea una página de registro, una de inicio de sesión y otra de perfil de usuario. Tomando como referencia varios ejemplos de zkoder<cite> [^3] [^4],</cite> se realiza la implementación de una autenticación basada en Token. Se prefiere la autentización mediante Token a la autenticación basada en Sesiones porque si en un futuro se implementa una aplicación nativa para móvil, la autenticación basada en sesiones no será compatible<cite> [^5].</cite> El prosósito de utilizar JWT es verificar que la información se origina de una fuente autenticada. En este caso, la autenticación de cada usuario. Para la creación de un JWT, se requiere una cabecera, una carga útil o _payload_ y una firma<cite> [^6].</cite>

[^3]: [{z}Koder, Node.js Express + Angular 8: JWT Authentication & Authorization example.](https://bezkoder.com/node-js-express-angular-jwt-auth/)
[^4]: [{z}Koder, Node.js Express: JWT example | Token Based Authentication & Authorization](https://bezkoder.com/node-js-jwt-authentication-mysql/)
[^5]: [{z}Koder, In-depth Introduction to JWT-JSON Web Token](https://bezkoder.com/jwt-json-web-token/)
[^6]: [Wikipedia, JSON Web Token](https://en.wikipedia.org/wiki/JSON_Web_Token)

La implementación se realiza con los módulos `jsonwebtoken` y `bcryptjs`. Para la instalación se ejecuta:

```bash
npm install jsonwebtoken bcryptjs --save 
``` 

### Intercambio de recursos de origen cruzado (CORS)

Se aplica el [**intercambio de recursos de origen cruzado o CORS**](https://es.wikipedia.org/wiki/Intercambio_de_recursos_de_origen_cruzado) (Cross-origin resource sharing) como mecanismo que permite que se puedan solicitar **recursos restringidos**. CORS define una forma en la cual el navegador y el servidor pueden interactuar para determinar si es seguro permitir una petición de origen cruzado. Esto permite tener más libertad y funcionalidad que las peticiones de mismo origen, pero es adicionalmente más seguro que simplemente permitir todas las peticiones de origen cruzado<cite> [^7].</cite> 

[^7]: [Wikipedia, Cross-origin resource sharing](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)

Se configura el `server.js` para activar CORS con `origin` en `http://localhost:8081`.

##  Crear, Leer, Actualizar y Borrar (CRUD)

Se implementan las funciones básicas de interacción con la base de datos o capa de persistencia. Concretamente para crear, leer, actualizar o borrar objetos, cuyo acrónimo es CRUD por sus siglas en inglés (Create, Read, Update and Delete). 

Para la implementación de la capa de persistencia se toma como referencia el ejemplo de zkoder<cite> [^8].</cite> Posteriormente a la creación de la base de datos PostgreSQL, se configura sequelize y la base de datos PostgeSQL como se ha explicado anteriormente, se inicializa y se define el modelo. Se crea un controlador para poder interactuar con los objetos de la base de datos. Cada función del controlador se trata de las funciones básicas de crear, leer, actualizar o borrar objetos de la base de datos y además se incluye una función para buscar objetos por el título de las donaciones. Por último, se definen las rutas de la API para poder ser solicitadas desde la capa de presentación.

[^8]: [{z}Koder, Node.js Express & PostgreSQL: CRUD Rest APIs example with Sequelize](https://bezkoder.com/node-express-sequelize-postgresql/)


## Asociaciones 

Las asociaciones y relaciones SQL entre tablas se realiza tomando como referencia la [documentación oficial de sequelize](https://sequelize.org/master/manual/assocs.html)<cite> [^9]</cite> y varios ejemplos de zkoder<cite> [^8,^10,^11].</cite> 

[^9]: [Sequelize Documentation, Associations](https://sequelize.org/master/manual/assocs.html)
[^10]: [{z}Koder, Node.js Sequelize Associations: One-to-Many example](https://bezkoder.com/sequelize-associate-one-to-many/)
[^11]: [{z}Koder, Node.js Sequelize Associations: Many-to-Many example](https://bezkoder.com/sequelize-associate-many-to-many/)


## Subida de imágenes

### Subida de múltiples imágenes

La implementación de subida de múltiples imágenes se realiza tomando como referencia varios ejemplos de zkoder<cite> [^12]</cite><cite> [^13]</cite><cite> [^14].</cite> Se utiliza el módulo npm `multer` como motor de almacenamiento en disco, se configura un _middleware_ para filtrar el tamaño máximo de cada archivo, la ruta donde se almacenará en el servidor y el cambio a un nuevo nombre. En la base de datos se almacenará la ruta de la imagen asociada al recurso para evitar una sobrecarga. Por último, queda pendiente la redimensión y compresión de las imágenes con `sharp`<cite> [^15].</cite>.

[^12]: [{z}Koder, Node.js Express File Upload Rest API example using Multer](https://bezkoder.com/angular-10-upload-multiple-images/)
[^13]: [{z}Koder, Upload/store images in MySQL using Node.js, Express & Multer](https://bezkoder.com/node-js-upload-image-mysql/)
[^14]: [{z}Koder, How to upload multiple files in Node.js](https://bezkoder.com/node-js-upload-multiple-files/)
[^15]: [{z}Koder, Upload & resize multiple images in Node.js using Express, Multer, Sharp](https://bezkoder.com/node-js-upload-resize-multiple-images/)


### Pruebas con ng2-file-upload

Se lleva a cabo una prueba siguiendo la [documentación oficial de ng2-file-upload](https://valor-software.com/ng2-file-upload/)<cite> [^16]</cite> y un tutorial de [positronX.io](https://www.positronx.io/angular-8-node-express-js-file-upload-tutorial/)<cite> [^17]</cite>. Se utiliza [multer](https://www.npmjs.com/package/multer) que es un **middleware** de node.js para el **manejo datos de formularios con multiparte**, el cual se se usa principalmente para **cargar archivos**. Se instala mediante el siguiente comando:

[^16]: [Valor Software, ng2-file-upload](https://valor-software.com/ng2-file-upload/)
[^17]: [PositronX.io, Angular 11 Node & Express JS File Upload Tutorial](https://www.positronx.io/angular-8-node-express-js-file-upload-tutorial/)

```bash
npm install multer --save
```

Se crea una nuevo archivo de rutas `routes/upload.routes.js` con el siguiente contenido:

```typescript
const multer = require('multer')

module.exports = app => {

  var router = require("express").Router();
  const PATH_IMAGES = './uploads';

  let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, PATH);
    },
    filename: (req, file, cb) => {
      cb(null, Date.now() + '-' + file.fieldname)
    }
  });

  let upload = multer({
    storage: storage
  });

  app.get('/api', function (req, res) {
    res.end('File catcher');
  });
  
  // POST File
  app.post('/api/upload', upload.single('image'), function (req, res) {
    if (!req.file) {
        console.log("No file is available!");
        return res.send({
            success: false
        });
  
    } else {
        console.log('File is available!');
        return res.send({
            success: true
        })
    }
  });

}
```

Para poder acceder a estas rutas es necesario importarlas en `server.js` con la siguiente línea:

```typescript
require('./routes/upload.routes')(app);
```

Finalmente este método se descarta por varios errores en la implementación.


## Variables de entorno

Se crea un archivo `.env` en el directorio raiz del proyecto con las variables de entorno que se vayan a utilizar. Por ejemplo<cite> [^17]</cite>:
[^18]: [John Papa, Node.js Everywhere with Environment Variables!](https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786)

```
PORT=8080
MODE_ENV=development
API_URL=localhost
```

Se instala el módulo npm `dotenv` con: 
```
npm install -s dotenv
```
Para leer el archivo se requiere importar el paquete `dotenv` y ejecutar la función `config()`. Mientras que para acceder a las variables desde el código JavaScript se utiliza `process.env.PORT`, `process.env.MODE_ENV`, `process.env.API_URL`. Un ejemplo para comprobar que lo anterior funciona sería viendo el resultado del siguiente log.

```
const dotenv = require('dotenv');
dotenv.config();
console.log(`Server is running in http://${process.env.API_URL}:${process.env.PORT} y mode ${process.env.MODE_ENV}.`);
```
La comunicación con la base de datos postgres se realiza mediante variables de entorno. En el archivo `./controllers/db.config.js` se leen las variables de entorno definidas en `.env` y se exportan para ser cargadas en sequelize en el archivo `./models/index.js`




