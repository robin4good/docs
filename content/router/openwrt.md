---
title: Instalación y configuración de OpenWrt
date: 2020-12-26
draft: false
weight: 0
---

{{< toc >}}

## Análisis del router Home Station Amper ASL26555

El router Alpha ASL-26555 se utiliza para ofrecer interconexión en red interna del clúster. Para proceder a la configuración del mismo se requiere conocer la configuración predeterminada. Se conecta el ordenador al router con un cable RJ-45 (Ethernet) y se accede desde un navegador a la dirección por defecto `192.168.1.1`. 

Al realizar un escaneo de puertos con `nmap`, se detectan muchos puertos abiertos innecesarios:
``` 
PORT      STATE    SERVICE
1/tcp     filtered tcpmux
21/tcp    open     ftp
23/tcp    open     telnet
80/tcp    open     http
139/tcp   open     netbios-ssn
443/tcp   open     https
445/tcp   open     microsoft-ds
515/tcp   open     printer
8000/tcp  open     http-alt
9100/tcp  open     jetdirect
50000/tcp open     ibm-db2
``` 

Se procede a realizar la configuración avanzada a través de la IP `http://192.168.1.1:8000/`. Al ser un router antiguo, lo primero a realizar es la actualización del firmware. Se observa que la versión es la v2.0.0.37B_ES. Buscando en internet no encontramos ningún sitio de descarga de firmwares de Alpha Network y leemos que sólo proporcionan los firmwares directamente a los proveedores, en este caso a Movistar. Además, buscando el nuevo firmware vemos que existe un [exploit](https://www.exploit-db.com/exploits/20667) que permite el acceso a la contraseña de administración remota.

Al conocer que el firmware es privativo, tener posibles vulnerabilidades y no tener mantenimiento actual, se encuentra una alternativa compatible de código abierto que es [OpenWrt](https://openwrt.org/). Por lo tanto se procede a la instalación del firmware OpenWrt más actualizado.

## Instalación de OpenWrt en ASL-26555

Movistar repartió dos modelos distintos, uno de memoria flash de 8MB, repartido en Chile, y otro de 16MB, en España, ambos están soportados por OpenWrt<cite> [^1].</cite> Para la instalación se requiere el archivo ejecutable del firmware, la herramienta mtd y un USB formateado en fat32. 
[^1]: [Wiki OpenWrt, Alpha ASL-26555.](https://oldwiki.archive.openwrt.org/toh/alpha/asl26555)

La descarga del firmware más actualizado se encuentra en la página de [descargas de OpenWrt](https://downloads.openwrt.org/), accedemos a la última release estable soportable (OpenWrt 19.07.5 fechada a Miércoles 9 Diciembre 2020), y que concuerde con el modelo que buscamos. En este caso se accede a `targets/ramips/rt305x` y se descarga el archivo [asl26555-16M-squashfs-sysupgrade.bin](https://downloads.openwrt.org/releases/19.07.5/targets/ramips/rt305x/openwrt-19.07.5-ramips-rt305x-asl26555-16M-squashfs-sysupgrade.bin), cuyo chesum es f22e331194fc1adf97ebdece9ae561843318db4efe22e60eb55e7ffaf4870abd. Una vez descargado, lo primero es comprobar el chesum para asegurarnos de que está íntegro y no ha sido modificado durante la descarga. El hash anterior se compara con el resultado del siguiente comando:
```bash 
sha256sum openwrt-19.07.5-ramips-rt305x-asl26555-16M-squashfs-sysupgrade.bin
``` 

La herramienta [mtd](https://openwrt.org/docs/techref/mtd) escribirá en la memoria el firmware OpenWrt en la memoria del router. Se descarga de la [página oficial de OpenWrt](https://openwrt.org/_media/media/alpha_networks/mtd-oem-asl-26555.tar.gz) para el dispositivo ASL-26555. Después de descargarlo, se descomprime el archivo.

### Downgrade previo del firmware oficial para evitar errores

Según [esta página web](https://cogemelamatricula.net/instalar-openwrt-en-el-router-asl-26555-de-movistar/)<cite> [^2]</cite>, con la versión del firmware oficial `v2.0.0.37B_ES`, existe un error al instalar OpenWrt que al reiniciar el router que no permitirá sobreescribir el firmware de OpenWrt. Por lo que es necesario hacer un _downgrade_ a la versión `v2.0.0.30B_ES`. Los pasos seguidos son los siguientes:
[^2]: [JM, Instalar OpenWrt en el router ASL-26555 de Movistar.](https://cogemelamatricula.net/instalar-openwrt-en-el-router-asl-26555-de-movistar/)
1. Descarga el [firmware oficial](https://cogemelamatricula.net/pub/generic_ASL-26555_ES_v2.0.0.30B_20110502.zip) de la versión `v2.0.0.30B_ES`. 
2. Descomprimir el archivo.
3. Entrar en la configuración avanzada [http://192.168.1.1:8000](http://192.168.1.1:8000), ir a la pestaña MAINTENANCE y en la sección Firmware Update seleccionar el archivo.
4. Pulsar en Update y esperar. 

Una vez terminado el proceso, ya tendrá la versión `v2.0.0.30B_ES`.

### Flasheo del firmware OpenWrt

Una vez descargados los dos archivos (mtd y openwrt-19.07.5-ramips-rt305x-asl26555-16M-squashfs-sysupgrade), se almacenan en un USB que esté formateado en `FAT32` y se conecta al router. Se conecta el router con el ordenador mediante ethernet y se asigna una IP manualmente dentro del rango 192.168.1.1/24. Entonces, nos conectamos al router mediante una sesión `telnet`:
```bash
telnet 192.168.1.1
```
Ahora es necesario moverse al directorio donde está montado el USB, que es `/var/tmp/storage_dev/usb1_1`:
```bash
cd /var/tmp/storage_dev/usb1_1
``` 
Se confirma que se encuentran los archivos con `ls` y se flashea la imagen de OpenWrt con:
```bash
./mtd write openwrt-19.07.5-ramips-rt305x-asl26555-16M-squashfs-sysupgrade.bin firmware
``` 
Una vez esperado el proceso, accedemos a la interfaz web a través de las direcciones `http://192.168.1.1` o `http://openwrt/`. Se recomienda apagar la interfaz wlan (WiFi) para prevenir interferencias entre redes.

## Configuración del router

Recordar que el servidor DHCP se encuentra en el nodo master y, por seguridad, para evitar el acceso a la configuración del router es necesario cambiar el rango de la subred LAN y desactivar el servidor DHCP.

### Acceso mediante SSH

Una vez instalado OpenWrt, se puede acceder al router mediante el comando:
```bash
ssh root@192.168.1.1
```
La primera vez no solicitará contraseña, por lo que lo primero a modificar es la contraseña mediante el comando `passwd`. Se recomienda poner una contraseña de más de 32 carácteres que sea alfanumbérica y con símbolos especiales.

Además el acceso de SSH se recomienda que sea a través de pares de claves público-privadas y desactivar el acceso mediante contraseña. Además, se cambiará el puerto por defecto por un puerto más alto. Para ello seguiremos los siguientes pasos:

+ Copiar la clave pública del ordenador con:
```bash
ssh-copy-id root@192.168.1.1
``` 
+ Desactivar el acceso mediante contraseña y elevación a un puerto superior:
```bash
uci set dropbear.@dropbear[0].Port='2222'
uci set dropbear.@dropbear[0].PasswordAuth='off'
uci set dropbear.@dropbear[0].RootPasswordAuth='off'
uci commit dropbear
/etc/init.d/dropbear reload
``` 
Otra forma de hacerlo sería modificando manualmente el archivo `/etc/config/dropbear`.

Por lo tanto, ahora se accederá mediante el comando:
```bash
ssh -p 2222 root@192.168.1.1
``` 

### Cambiar el rango de la subred LAN

Por defecto, el rango de subred de la LAN es el 192.168.1.1/24, para cambiarlo no se recomienda realizarlo a través de la interfaz gráfica de LuCI porque no se guardan los cambios. Por lo tanto accedemos mediante ssh al router y ejecutamos los siguientes comandos:

```bash
vim /etc/config/network
``` 
Pulsar la tecla escape `ESC` y escribir `:wq` para guardar los cambios. Reiniciamos el servicio de red con:
```bash
/etc/init.d/network restart
``` 
Perderemos la conexión a través de ssh debido a que se ha modificado la IP. Ahora, será necesario conectarse al router a través de ssh con la nueva IP.

Alternativamente, también se puede realizar usando `uci`:
```bash
uci set network.lan.ipaddr='192.168.100.1'
uci commit
/etc/init.d/network restart
```

### Desactivación del servidor DHCP

Para desactivar el servidor DHCP en el router y dejar que se encargue de la asignación de IP el servidor DHCP utilizado en el nodo master, ejecutar lo siguiente en el router:
```bash
uci set dhcp.lan.dynamicdhcp=0
uci commit dhcp
service dnsmasq restart
```
Otra opción para realizar la desactivación de DHCP sería modificando manualmente el archivo `/etc/config/dhcp` y añadiendo un 0 en la opción `dhcp.lan.dynamicdhcp=0`. Para actualizar el servicio ejecutar `service dnsmasq restart`.

## Backup de la configuración

Este paso se realizará desde la interfaz gráfica LuCI. Para hacerlo, ir a la sección `System > Backup` y pulsar `Generate archive`.

## Restauración de la configuración a los valores de fábrica

En caso de obtener algún error durante la configuración y no poder acceder a la interfaz gráfica de configuración o acceso por ssh, se pueden seguir los siguientes pasos para restaurar los valores de fábrica.

Es necesario utilizar el `modo failsafe` para restaurar los valores de fábrica en un router OpenWrt. Se recomienda apagar la interfaz wlan (WiFi) para prevenir interferencias entre redes.

1. Se conecta a la corriente y justo al encenderlo se presiona el botón de reset durante unos 10-15 segundos hasta que el botón de Power se encienda de color rojo y empiece a parpadear rápidamente. El router entrará en `modo failsafe`. 

2. Se conecta con un cable RJ-45 (Ethernet) a un ordenador y se asigna una IP a la interfaz de ethernet del ordenador. Al entrar en `modo failsafe` el router se asignará la IP 192.168.1.1, por lo que es necesario asignarse una IP dentro de esa red. Primero, mirar cuál es la interfaz del ethernet, podría ser `eth0` o `enp2s0`. Una vez conocida la interfaz, se puede asignar una ip manualmente con el programa de redes de interfaz gráfica o con el siguiente comando:
```bash
sudo ip addr add 192.168.1.100/24 dev enp2s0
```

3. Accedemos al router mediante ssh:
```bash
ssh root@192.168.1.1
``` 

4. Ejecutamos el comando `firstboot` y se responde `y` para que se empiece a resetear.

5. Reiniciamos el router con `reboot` y ya podemos acceder al router mediante el navegador y la dirección `192.168.1.1`.

