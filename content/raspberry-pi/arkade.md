---
title: Arkade
date: 2020-12-11
draft: false
weight: 26
---

{{< toc >}}


Se sigue la referencia de la entrada de Alex Ellis, el creador de [Arkade](https://github.com/alexellis/arkade), llamada "arkade by example — apps for Kubernetes, the easy way"<cite> [^1]</cite>.

[^1]: [Alex Ellis,  arkade by example — apps for Kubernetes, the easy way](https://itnext.io/kubernetes-apps-the-easy-way-f06d9e5cad3c)


## Instalación

Se instala mediante el comando<cite> [^2]:</cite>.

[^2]: [Arkade GitHub,  arkade - The Open Source Kubernetes Marketplace](https://github.com/alexellis/arkade)


```bash
curl -sLS https://dl.get-arkade.dev | sudo sh
```
Para mostrar los paquetes que se pueden instalar se ejecuta:
```bash
ark install --help
```

## IngressController

Al ser en una máquina local se utiliza:
```
arkade install nginx-ingress --host-mode
```


## Grafana

Se instala grafana con el siguiente mandato:
```
ark install grafana
```

Se obtiene la contraseña a través del comando:

```
k get secret --namespace grafana grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

Se realiza una redirección de puertos mediante:

```
k --namespace grafana port-forward service/grafana 3000:80
```

Se accede a través de ssh redireccionando los puertos con:
```
sh -L3000:localhost:3000 -p 62626 pi@192.168.1.85
```

Se accede mediante el usuario `admin` y la contraseña obtenida anteriormente.

## Paquetes no soportados por ARM

- postgresql. Sólo es soportado para procesadores intel.
- gitea. Se requiere una base de datos externa.
- gitlab. Se requiere dominio e IP-externa.