---
title: Configuración de la red NAT hacia el exterior
date: 2020-12-11
draft: false
weight: 5
---

{{< toc >}}

## ¿Cómo se mapean las direcciones IP a nombres más legibles?

Para manejar más facilmente el clúster, se recomienda realizar un mapeo de nombres a las direcciones IP de las máquinas. Para ello se añade en cada máquina en el fichero `/etc/hosts` lo siguiente: 

```
10.0.0.1 master.node master m
10.0.0.2 worker.node1 worker1 w1 
10.0.0.3 worker.node2 worker2 w2
```

## ¿Cómo configurar la red NAT hacia el exterior?

El último paso en la configuración de la red es configurar la traducción de direcciones de red (NAT) para que sus nodos puedan acceder a la Internet pública (en caso de desear que puedan hacerlo). El funcionamiento será como un **router**, es decir, **recibiendo paquetes, decidiciendo la ruta de estos y reenviarlos por cualquiera de las interfaces de red existentes**. Para hacerlo se necesita modificar el parámetro del kernel denominado `ip_forward`. Se edita el fichero `/etc/sysctl.conf` y se añade<cite> [^2]:</cite>
[^2]: [O'Reilly Media, Kubernetes: Up and Running](https://www.oreilly.com/library/view/kubernetes-up-and/9781492046523/)

```
net.ipv4.ip_forward=1
``` 

Para que surta efecto se puede reiniciar el sistema con `sudo reboot` o directamente ejecutar `sudo sysctl net.ipv4.ip_forward=1`. En caso de realizar el último paso, es necesario editar el fichero `/etc/sysctl.conf` para que los cambios sean permanentes. 

Se edita el fichero  `/etc/rc.local` y se añaden las reglas de **iptables** para efectuar el reenvío desde `eth0` a `wlan0` y viceversa. El archivo `/etc/rc.local` sirve para ejecutar comandos o programas durante el inicio de la Raspberry Pi. Dar permisos de ejecución al archivo con `sudo chmod +x /etc/rc.local`. El contenido a escribir es el siguiente:

```bash
#!/bin/sh -e
iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
iptables -A FORWARD -i wlan0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i eth0 -o wlan0 -j ACCEPT
exit 0
```
Para comprobar el estado del servicio `rc.local` se utiliza:
```
sudo systemctl status rc.local.service
```
Por defecto se encuentra inactivo y mostrará un aviso diciendo que se ha modificado el fichero `/etc/rc.local` y que se requiere recargar el demonio del sistema con :
```
sudo systemctl daemon-reload
```
Lo siguiente es levantar el servicio `rc.local` con:
```
sudo systemctl restart rc.local.service
```
Y comprobar que no haya ningún error de compatibilidad con:
```
sudo systemctl status rc.local.service
```
A partir de este momento, se ejecutarán los comandos de iptables en el inicio de las máquinas. Para comprobarlo reiniciar y volver a comprobar el estado del servicio.

En caso de no querer reiniciar el sistema se puede ejecutar directamente con permisos de root:
```
sudo iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
sudo iptables -A FORWARD -i wlan0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth0 -o wlan0 -j ACCEPT
```

Los flags y opciones anteriores tienen los siguientes significados<cite> [^3]:</cite>
[^3]: [K. Rupp, NAT - Network Address Translation](https://www.karlrupp.net/en/computer/nat_tutorial)

- iptables: cli para modificar o filtrar la red a nivel del kernel de Linux con el frawework netfilter<cite> [^4].</cite>
- -t nat: selecciona tabla NAT para configurar las reglas.
- -A POSTROUTING: añade la regla a la cadena POSTROUTING.
- -A FORWARD: añade la regla a la cadena FORWARD.
- -o wlan0/eth0: regla válida para los paquetes de salida (output) con la interfaz indicada.
- -i wlan0/eth0: regla válida para los paquetes de entrada (input) con la interfaz indicada.
- -j MASQUERADE: sustituir la máscara del paquete. En este caso de la dirección del remitente por la de la nueva interfaz.
- -j ACCEPT: el objetivo ha sido aceptado.
- -m state --state RELATED,ESTABLISHED: estado asignado a un paquete que está iniciando una nueva conexión y está asociado a una conexión (RELATED) o a un paquete con una conexión ya establecida y totalmente válida<cite> [^5]</cite>
[^4]: [Wikipedia, Netfilter](https://en.wikipedia.org/wiki/Netfilter)

[^5]: [Oskar Andreasson, Iptables Tutorial 1.2.2](https://book.huihoo.com/iptables-tutorial/x6330.htm#TABLE.STATEMATCHES)

Se puede validar lo anterior mirando el fichero `/var/lib/dhcp/dhcpd.leases`. También se puede validar probando que los nodos pueden conectarse a Internet o a través de los siguientes comandos:
```
sudo iptables -L
```
De modo que debe mostrar lo siguiente:
```
...
Chain FORWARD (policy ACCEPT)
target     prot opt source               destination
DOCKER-USER  all  --  anywhere             anywhere            
DOCKER-ISOLATION-STAGE-1  all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             state RELATED,ESTABLISHED
ACCEPT     all  --  anywhere             anywhere            
...
```

Mientras que el comando:
```
sudo iptables -t nat -L
```
Debe mostrar lo siguiente:
```
...
Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination         
MASQUERADE  all  --  172.17.0.0/16        anywhere            
MASQUERADE  all  --  anywhere             anywhere  
...  
```