---
title: Helm en ARM
date: 2020-12-11
draft: false
weight: 24
---

{{< toc >}}

Helm es el manejador de paquetes recomendado para Kubernetes.

## Instalación desde el binario

Se escoge y se descarga el último paquete de Helm de ARM en su repositorio de [realease](https://github.com/helm/helm/releases)<cite> [^1]</cite>:

[^1]: [Helm Documentation,  Installing Helm ](https://helm.sh/docs/intro/install/)

```
wget https://get.helm.sh/helm-v3.5.0-linux-arm.tar.gz
```
Se descomprime con:
```
tar xvf helm-v3.5.0-linux-arm.tar.gz
```
Se mueve el binario ejecutable a la carperta `/usr/local/bin`:
```
sudo mv helm linux-arm/usr/local/bin/
```
Se comprueba que está instalado con:
```
helm version
```
Se actualiza el repositorio con:
```
helm repo update
```
En caso de obtener el error de que el cluster es innacesible por estar usando k3s<cite> [^2]</cite>, se exporta la variable de entorno `KUBECONFIG` asociándola al fichero de configuración de k3s.

[^2]: [k3s.io GitHub,  Error: Kubernetes cluster unreachable with helm 3.0 #1126 ](https://github.com/k3s-io/k3s/issues/1126)

```bash
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
```
Se dan permisos de ejecución con:
```bash
sudo chmod 407 /etc/rancher/k3s/k3s.yaml
```

## Despliegue de una aplicación

### Postgres
Por ejemplo para desplegar el chart de bitnami de postgresql se utiliza<cite> [^3]</cite>:
[^3]: [Bitnami GitHub, Chart PostgreSQL](https://github.com/bitnami/charts/tree/master/bitnami/postgresql/#installing-the-chart)

```
helm install postgres-release bitnami/postgresql
```

En caso de querer añadir parámetros en la instalación se haría de este modo:
```
helm install postgres-release --set postgresqlUsername=username,postgresqlPassword=secretpassword,postgresqlDatabase=database_name bitnami/postgresql
```

El comando anterior devuelve varios valores importantes: 
+ Se indica que puede ser accedida por el puerto 5432 con la resolución de DNS desde el cluster `postgres-release-postgresql.testing.svc.cluster.local`. La conexión es de lectura/escritura.

+ Para obtener la contraseña del usuario `postgres` (usuario administrador) se ejecuta:
```bash
export POSTGRES_ADMIN_PASSWORD=$(kubectl get secret --namespace testing postgres-release-postgresql -o jsonpath="{.data.postgresql-postgres-password}" | base64 --decode)
```

+ Para obtener la contraseña de `postgres` (usuario sin privilegios) se ejecuta:
```bash
export POSTGRES_PASSWORD=$(kubectl get secret --namespace testing postgres-release-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
```

+ Para conectar con la ejecución de la base de datos ejecutar:
```bash
kubectl run postgres-release-postgresql-client --rm --tty -i --restart='Never' --namespace testing --image docker.io/bitnami/postgresql:11.10.0-debian-10-r79 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host postgres-release-postgresql -U username -d database_name -p 5432
```

+ Para conectar con la base de datos desde el exterior del cluster ejecutar:
```bash
kubectl port-forward --namespace testing svc/postgres-release-postgresql 5432:5432 &
    PGPASSWORD="$POSTGRES_PASSWORD" psql --host 127.0.0.1 -U username -d database_name -p 5432
```

{{< hint danger >}}
**ERROR: Se provocan errores al levantar los pods debido a que las imágenes de Bitnami no soportan la arquitectura ARM.** 
{{< /hint >}}


Se comprueba la lista de charts mediante:
```
helm list
```

### Prometheus

El chart de helm de prometheus está deprecado, por lo que se requiere instalar el helm mantenido por la comunidad<cite> [^4]</cite>. Primero se agrega el repositorio, se actualizan los repositorios de helm y se buscan entre los paquetes de la comunidad de prometheus:

[^4]: [Prometheus-community  GitHub, Prometheus Community Kubernetes Helm Charts](https://github.com/prometheus-community/helm-charts)


```
helm repo update
helm search repo prometheus-community
```

Buscando los paquetes en el repositorio de prometheus se encuentra el paquete `prometheus-community/prometheus`.

```bash
helm install prometheus prometheus-community/prometheus
```

```
export POD_NAME=$(k get pods --namespace default -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
```

Se redirigen el tráfico al puerto 9090:
```
k --namespace default port-forward $POD_NAME 9090
```

## Desintalación de una aplicación

Se elimina la aplicación con:

```
helm delete postgres-release
```

Pero los componentes PVC's asociados al chart no se eliminan y se requiere hacerlo manualmente con:
```
kubectl delete pvc -l release=postgres-release
```
