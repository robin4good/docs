---
title: Instalación de HypriotOS
date: 2020-12-07
draft: false
weight: -10
---

{{< toc >}}

Al querer usar Docker en una Raspberry Pi, lo primero a tener en cuenta es que la arquitectura de CPU es ARM en vez de x86/x64 de Intel o AMD. Por lo tanto, las aplicaciones basadas en Docker deben estar empaquetadas específicamente para la arquitectura ARM. Esto hay que tenerlo en cuenta a la hora de buscar aplicaciones en Docker Hub, donde hay que buscar aplicaciones donde aparezcan las palabras RPI o ARM en el título o descripción.

Docker no viene preinstalado en Raspbian. Por lo tanto, estudiando los distintos sistemas operativos adecuados para utilizar Docker en una Raspbery Pi, HypriotOS parece ser la mejor alternativa. 

## ¿Qué es HypriotOS?

HypriotOS es un sistema operativo basado Raspbian, que a su vez está basado en Debian, optimizado para ejecutar Docker en plataformas ARM como las Raspberry Pi.

## ¿Cómo se flashea la imagen?

Para quemar la imagen dentro de la tarjeta microSD, existen diversas maneras de hacerlo. En nuestro caso hemos usado la herramienta [flash](https://github.com/hypriot/flash) instalada desde los repositorios oficiales de Arch. 

Una vez instalada la herramienta para quemar la imagen, se copia el enlace de la imagen ISO comprimida desde la [página oficial de HypriotOS](https://blog.hypriot.com/downloads/) o se descarga la imagen. En caso de descargar la imagen es necesario verificar que el checksum es el mismo con los siguientes comandos:

```bash
sha256sum hypriotos-rpi-v1.12.3.img.zip
cat hypriotos-rpi-v1.12.3.img.zip.sha256
```
Una vez verificado el checksum se puede flasear la imagen con el comando a continuación. Aunque se recomienda leer la opción de configurar el WiFi antes de quemar la imagen.
```bash
sudo flash hypriotos-rpi-v1.10.0.img.zip
```

Para quemar la imagen con el enlace directo, ejecutar:

```bash
sudo flash https://github.com/hypriot/image-builder-rpi/releases/download/v1.12.3/hypriotos-rpi-v1.12.3.img.zip
```

Solicitará la ubicación de la partición de la tarjeta SD y una confirmación. Una vez termine el proceso, se introducirá la SD en la Raspberry Pi y ya está lista para usar.

Lo primero de todo al iniciar el sistema operativo será cambiar la contraseña por defecto.

### ¿Cómo configurar el WiFi antes de flashear la imagen?

Primero creamos un archivo llamado wifi.yaml con la siguiente plantilla, donde es necesario modificar el SSID del WiFi y la clave precompartida en texto plano o cifrada con `wpa_passphrase SSID passwordWiFi`. En caso de añadir la clave en texto plano será necesario ponerla entre comillas `psk="ContraseñaWiFi"`, mientras que si se añade cifrada no serán necesarias las comillas `psk=jghXt58cfvfgbksFSJhoA`. Para conseguir la contraseña cifrada es necesario ejecutar en un terminal `wpa_passphrase "NOMBRE_WIFI"` pedirá la contraseña en texto plano y devolverá la contraseña cifrada.

```yaml
#cloud-config
# vim: syntax=yaml
#

# Set your hostname here, the manage_etc_hosts will update the hosts file entries as well
hostname: master-node
manage_etc_hosts: true

# You could modify this for your own user information
users:
  - name: pi
    gecos: "Hypriot Pirate"
    sudo: ALL=(ALL) NOPASSWD:ALL
    shell: /bin/bash
    groups: users,docker,video,input
    plain_text_passwd: hypriot
    lock_passwd: false
    ssh_pwauth: true
    chpasswd: { expire: false }

# # Set the locale of the system
locale: "en_US.UTF-8"

# # Set the timezone
# # Value of 'timezone' must exist in /usr/share/zoneinfo
timezone: "Europe/Madrid"

# # Update apt packages on first boot
# package_update: true
# package_upgrade: true
# package_reboot_if_required: true
package_upgrade: false

# # Install any additional apt packages you need here
# packages:
#  - ntp

# # WiFi connect to HotSpot
# # - use `wpa_passphrase SSID PASSWORD` to encrypt the psk
write_files:
   - content: |
       allow-hotplug wlan0
       iface wlan0 inet dhcp
       wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
       iface default inet dhcp
     path: /etc/network/interfaces.d/wlan0
   - content: |
       country=es
       ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
       update_config=1
       network={
       ssid="NOMBRE_WIFI"
       psk="ContraseñaWiFi"
       proto=RSN
       key_mgmt=WPA-PSK
       pairwise=CCMP
       auth_alg=OPEN
       }
     path: /etc/wpa_supplicant/wpa_supplicant.conf

# These commands will be ran once on first boot only
runcmd:
  # Pickup the hostname changes
  - 'systemctl restart avahi-daemon'

#  # Activate WiFi interface
  - 'ifup wlan0'
```

Una vez se haya descargado la imagen de HypriotOS y se haya configurado la platilla de configuración inicial guardada como `wifi.yaml`. Desde el mismo directorio que almacena los dos archivos ejecutamos:

```bash
sudo flash --hostname master-node --userdata wifi.yaml hypriotos-rpi-v1.12.3.img.zip
```
Donde el flag `--hostname` sirve para cambiar el nombre de la máquina y el flag `--userdata` sirve para cargar la configuración inicial.

En caso de querer cambiar el `hostname` después de la instalación, usar el siguiente comando:
```bash
sudo hostnamectl set-hostname --static nuevohostname
```
Además, para que sea un cambio persistenten es necesario modificar el archivo `sudo nano /etc/cloud/cloud.cfg` con lo siguiente:
```
preserve_hostname: true
```
Después comprobar que se ha cambiado el `hostname` en `/etc/hostname` y en `/etc/hosts`, si no es así modificarlo manualmente. Por último, reiniciar el sistema. Todo lo anterior es necesario debido a un error a partir de la versión de Hypriot 1.11<cite> [^1].</cite>
[^1]: [Hypriot GitHub. Issue: hostname can not be changed using normal CLI methods](https://github.com/hypriot/image-builder-rpi/issues/309)

### ¿Cómo automatizar el flasheo de la imagen?

Si no se quiere un uso interactivo del comando, se puede automatizar la quema de imágenes mediante el comando:
```bash
flash -d /dev/mmcblk0 -f hypriotos-rpi-v1.12.0.img
```

## ¿Cómo localizar la IP local de la Rasberry Pi?

Primero ver a qué red local estamos conectados con desde la máquina host:

```bash
ip a
```
ó 
```bash
sudo ip addr show
```

Una vez que tengamos el rango de la subred (habitualmente siempre es por defecto la 192.168.1.0/24), realizamos un escaneo de red con `nmap`:

```bash
sudo nmap -f 192.168.1.0/24
```

El flag `-f` es para realizar el escaneo rápido y poder averiguar la IP de la Raspberry mediante los dígitos fijos de la MAC del fabricante.

## ¿Cómo acceder mediante ssh?

Una vez se haya localizado la IP privada de la Raspberry y se conozca cuál es el usuario creado durante la instalación (usuario `pirate` y contraseña `hypriot` por defecto), ejecutamos lo siguiente:

```bash
ssh pirate@<ip_privada_raspberry_pi>
#En nuestro caso hemos cambiado el usuario a pi y la IP es la 192.168.1.50
ssh pi@192.168.1.50
```

## ¿Cómo cambiar la contraseña por defecto y actualizar el sistema?

Para cambiar la contraseña se utiliza el comando:
```bash
passwd
```
Mientras que para actualizar el sistema se utiliza:
```bash
sudo apt update
sudp apt upgrade
```

## ¿Cómo comprobar que Docker viene preinstalado?

Mediante el comando:
```bash
docker info
``` 

## ¿Cómo apagar la raspberry pi de forma correcta?

Ejecutando el comando:

```bash 
sudo shutdown -h now
```

Esperar a que la luz verde deje de parpadear y ya se puede desenchufar el cable de alimentación.
