---
title: Configuración de acceso remoto SSH
date: 2020-14-11
draft: false
weight: -5
---

{{< toc >}}

## ¿Cómo configurar SSH a través de claves público-privada en vez de contraseña?

Desde la máquina host utilizada para la configuración de las Raspberrys Pi y desde la propia Raspberry Pi, al ejecutar el siguiente comando se generarán dos claves, una pública y otra privada:
```bash
ssh-keygen -o -a 100 -t ed25519 -f $HOME/.ssh/id_ed25519 -C "pi@master-node"
```
Por defecto en el paso `Enter file in which to save the key (/home/pi/.ssh/id_ed25519):` guardará las claves en `/home/pi/.ssh/id_ed25519` y `/home/pi/.ssh/id_ed25519.pub`. Seguidamente pedirá una contraseña y una vez generadas las claves, copiar el contenido de la clave pública `$HOME/.ssh/id_ed25519.pub` en el fichero `/home/pi/.ssh/authorized_keys` de cada nodo del clúster. Para ello, se recomienda utilizar el siguiente comando desde el ordenador:
```bash
ssh-copy-id -i $HOME/.ssh/id_ed25519.pub pi@<ip de la raspberry>
```
Solicitará la contraseña de la clave SSH y se conectará a la Raspberry Pi.

A partir de ahora, cada vez que se acceda por SSH a algún nodo, por defecto se pedirá la contraseña de la clave privada SSH en vez de la contraseña de sesión del nodo.

Existen dos ficheros de configuración de OpenSSH:
- El archivo de configuración del **servidor** `sshd_config` alojado en `/etc/ssh/`.
- El archivo de configuración del **cliente** `ssh_config` alojado en `/etc/ssh/`

Lo último, será restringir el acceso remoto con contraseñas. De manera que sólo se podrán conectar remotamente a las máquinas aquellas que hayan subido previamente su clave pública SSH a cada Rasberry Pi. Para realizarlo, es necesario editar el fichero de configuración `sshd_config` de la Raspberry Pi y modificar la línea `PasswordAuthentication no`. 

```bash
sudo nano /etc/ssh/sshd_config
```
En la línea `PasswordAuthentication` se modifica a `no`:
```
PasswordAuthentication no
``` 
Otra manera de llevarlo a cabo es modificando el fichero de configuración global `$HOME/etc/ssh/ssh_config` con los siguientes parámetros<cite> [^1]:</cite>
[^1]: [Vivek Gite, Top 20 OpenSSH Server Best Security Practices](https://www.cyberciti.biz/tips/linux-unix-bsd-openssh-server-best-practices.html)

```
Port 2251
PermitRootLogin no
ChallengeResponseAuthentication no
PasswordAuthentication no
UsePAM no
AuthenticationMethods publickey
PubkeyAuthentication yes
AllowUsers pi
X11Forwarding no
Banner /etc/issue
```
Se valida i la configuración del fichero `ssh_config` es correcta con los siguientes comandos:

```
#Versión simple
sudo sshd -t
#Versión extendida
sudo sshd -T
```
En caso de obtener el error ``, ejecutar:
```
#Versión simple
sudo /usr/sbin/sshd -t
#Versión extendida
sudo /usr/sbin/sshd -T
```

Se guardan los cambios y se reinicia el servicio con:
```bash
sudo systemctl start ssh
```

## ¿Cómo cambiar el puerto por defecto de las conexiones SSH?

Una práctica de seguridad habitual es cambiar el puerto por defecto de SSH (puerto 22). Esto es porque durante un ataque, probablemente se pruebe la conexión por ese puerto. De esta manera, al cambiar el puerto, se reducen el número de intentos de conexión por parte de posibles atacantes<cite> [^2].</cite>
[^2]: [Seguridad de SSH - Parte IV Securicemos nuestra servidora web | la_bekka](https://labekka.red/servidoras-feministas/2019/10/09/fanzine-parte-4.html#ssh)

Se puede escoger un puerto alto entre el 1024 hasta el 65535, para evitar los escaneos rápidos de red de los puertos más comunes. Antes de seleccionar un puerto, es necesario revisar que no haya otro servicio utilizando el mismo puerto. Para ello se ejecuta el comando:
```bash
netstat -ltnp
```
Donde los flags son:
+ `l` muestra sólo los sockets a la escucha
+ `t` muestra las conexiones tcp 
+ `n` muestra las direcciones de forma numérica
+ `p` muestra los identificadores de procesos/programa

Una vez comprobado que el puerto seleccionado no está en uso se modifica el archivo de configuración de SSH:
```bash
sudo nano /etc/ssh/sshd_config
```
Se cambia la línea `#Port 22` por otro puerto, por ejemplo, `Port 2251`. Se guarda el fichero y se reinicia el servicio con:
```bash
sudo service ssh restart
```
En caso de haber un error en el proceso de cambio de puerto, se cerrará la sesión ssh de la máquina y se perderá el acceso. Por lo tanto, es recomendable estar físicamente cerca de la máquina.

A partir de este momento, para acceder vía SSH a cada Raspberry Pi será necesario especificar el puerto modificado. El mandato a ejecutar será:
```bash
ssh -p 2251 pi@<dirección ip>
```
En caso de olvidar el puerto escogido, se puede hallar mediante un escaneo de puertos con el comando:

```bash
sudo nmap -p 1-65535 -T4 -A -v <ip-servidora>
```

## ¿Cómo mapear IPs a través de nombres?

Modificando el archivo `/etc/hosts` para añadir las asociaciones de IPs con nombres de las máquinas. Se añadirán tantas IP y nombres de máquinas como nodos del clúster haya. Es necesario añadirlo en todos los nodos y servirá para manejar las máquinas de manera más sencilla.

```
192.168.1.150 master.node master m
192.168.1.151 worker.node1 node1 w1
192.168.1.152 worker.node2 node2 w2
```
Una vez esté modificado, podremos acceder desde el nodo máster al primer nodo woker por ssh de la siguiente manera:
```ssh
ssh pi@worker1
ssh pi@w1
```

## ¿Cómo configurar el cliente de SSH?

Desde la máquina que se quiera conectar a las raspberrys pis, se puede modificar la configuración de SSH para que sea más sencillo y todo esté preconfigurado. Existen dos archivos de configuración que no están creados por defecto: `$HOME/.ssh/config` y `$HOME/.ssh/ssh_config`. El fichero SSH `$HOME/.ssh/config` es para el usuario, debe crearse con los permisos mínimos para que sólo sea leído o modificado por el usuario. Mientras que el archivo `/etc/ssh/ssh_config` es global para todos los usuarios<cite> [^3].</cite>
```
touch ~/.ssh/config && chmod 600 ~/.ssh/config
```
[^3]: [ssh.com, SSH Config File](https://www.ssh.com/ssh/config/)

En el archivo creado introducimos lo siguiente:
```
Host *
  PreferredAuthentications publickey,keyboard-interactive

Host master m
  User pi
  Hostname master.node
  Port 2251
  ServerAliveInterval 120
  ServerAliveCountMax 30

Host worker1 w1
  User pi
  Hostname worker.node1
  Port 2251
  ServerAliveInterval 120
  ServerAliveCountMax 30

Host worker2 w2
  User pi
  Hostname worker.node1
  Port 2251
  ServerAliveInterval 120
  ServerAliveCountMax 30
```
De manera que las conexiones se pueden realizar mediante:
```
ssh m
ssh w1
ssh w2
ssh master
ssh worker1
ssh worker2
```