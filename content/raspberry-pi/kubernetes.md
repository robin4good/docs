---
title: Instalación de Kubernetes y configuración del clúster
date: 2020-12-11
draft: false
weight: 20
---

{{< toc >}}

Una vez que sean accesibles todos los nodos mediante SSH y tengan acceso a Internet, es el momento de instalar Kubernetes en todos ellos. 
## ¿Cómo instalar Kubernetes?
Para instalar Kubernetes se requiere privilegios de `root`.
```bash
sudo su -
```
Para instalar Kubernetes y sus dependencias, es necesario obtener la clave APT de kubernetes y agregar el repositorio APT oficial de Kubernetes en cada nodo. Después se actualizarán los repositorios y se instalarán los paquetes `kubeadm, kubelet y kubectl` en todas las máquinas. Por último, se marcarán como `hold` para evitar que los paquetes se instalen, actualicen o se eliminen de forma automática<cite> [^1].</cite>
[^1]: [Kubernetes Documentation. Installing kubeadm, kubelet and kubectl](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl)

+ `kubeadm` comando para iniciar y administrar el clúster.
+ `kubelet` componente que se ejecuta en todas las máquinas del clúster y realiza tareas como iniciar pods y contenedores.
+ `kubectl` herramienta de línea de comandos para comunicarse con el clúster.

```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```

## ¿Cómo habilitar el autocompletado del intérprete de comandos?

Un paso muy habitual después de la instalación, es configurar y habilitar el autocompletado en el intérprete de comandos de **kubectl**<cite> [^2]:</cite>
[^2]: [Kubernetes Documentation. Enabling shell autocompletion](https://kubernetes.io/docs/tasks/tools/install-kubectl/#enabling-shell-autocompletion)

```bash
sudo apt install -y bash-completion
source /usr/share/bash-completion/bash_completion
source <(kubectl completion bash)
echo 'source <(kubectl completion bash)' >>~/.bashrc
```

## ¿Cómo configurar la red del clúster?

En el **nodo master** (donde se está ejecutando DHCP y tiene conexión a Internet), ejecutar:

```bash
sudo kubeadm init --pod-network-cidr 10.244.0.0/16  --apiserver-advertise-address 10.0.0.1  --apiserver-cert-extra-sans kubernetes.cluster.home
```
Los flags utilizados son:
+ `--pod-network-cidr` especifica el **rango de direcciones IP para la red de Pods**. Con este comando iniciará el plano de control (control-plane)<cite> [^3].</cite> 
+ `--apiserver-advertise-address` indica la **dirección IP en la que el servidor API anunciará que está a la escucha**<cite> [^3].</cite>
+ `--apiserver-cert-extra-sans` utiliza **nombres adicionales SANs (Subject Alternative Names) para utilizar con el certificado de servicio del servidor API**. Pueden ser direcciones IP y nombres DNS<cite> [^3].</cite>

Además, concretamente para la versión 1.20.0 de Kubernetes, es necesario añadir el flag `--ignore-preflight-errors=Mem` para evitar un bug que produce un error de memoria. En las versiones posteriores estará arreglado<cite> [^4].</cite>

Es importante usar el comando del flag `--pod-network-cidr` con el rango de direcciones que se expone porque se usa [Flannel](https://coreos.com/flannel/docs/latest/) como una subred virtual de cada host para el entorno de contenedores. Se utiliza para almacenar metadatos sobre las asignaciones de las direcciones IP del rango CIDR de los Pods<cite> [^5].</cite> Además, es importante realizar una copia segura del comando `kubeadm join` que se ha devuelto porque servirá para añadir nodos al clúster<cite> [^6].</cite> En caso de equivocación o de querer deshacer los cambios aplicados con `kubeadm init` ejecutar `kubeadm reset`.

[^3]: [Kubernetes Documentation. kubeadm init](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/)
[^4]: [GitHub kubeadm. Kubeadm init 1700MB limit bug](https://github.com/kubernetes/kubeadm/issues/2365)
[^5]: [HypriotOS Blog. Setup Kubernetes on a Raspberry Pi Cluster easily the official way!](https://blog.hypriot.com/post/setup-kubernetes-raspberry-pi-cluster/)
[^6]: [Kubernetes Documentation. Creating a cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)

Para comenzar a usar su clúster, se ejecuta desde el nodo master lo siguiente con usuario habitual:
```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
Alternativamente, si es el usuario root, se puede ejecutar:
```bash
export KUBECONFIG=/etc/kubernetes/admin.conf
```
Ahora, se añaden los demás nodos trabajadores con el comando que había devuelto `kubeadm init`. El comando `kubeadm join` tiene esta estructura:

```bash
kubeadm join <control-plane-host>:<control-plane-port> --token <token> --discovery-token-ca-cert-hash sha256:<hash>
kubeadm token list
```

En caso de que caduque el token, es posible generar otro nuevo desde el nodo master con el siguiente comando:

```bash
kubeadm token create --print-join-command
``` 

### ¿Cómo configurar la red a nivel Pod a Pod?

Una vez configurada la red a nivel de nodo, el siguiente paso es configurar la red Pod a Pod. Como en principio, todos los nodos del clúster se ejecutan en la misma red interna a través de Ethernet, simplemente se configuran las reglas de enrutamiento en los kernels de cada máquina. La manera más sencilla de usar un **controlador de red del Pod es configurando la herramienta** [**Flannel**](https://github.com/coreos/flannel), creada por CoreOS. De los distintos modos de enrutamientos permitidos por Flannel, se usará `host-gw` porque es recomendado para proporcionar un buen rendimiento, con pocas dependencias y una configuración sencilla. `host-gw` sirve para crear rutas IP a subredes a través de IP de máquinas remotas. Requiere conectividad directa de la capa 2 entre máquinas que ejecutan Flannel<cite> [^7].</cite> Para ello, nos descargamos una configuración de ejemplo:
[^7]: [Flannel Documentation. Backends](https://coreos.com/flannel/docs/latest/backends.html)

```bash
curl https://rawgit.com/coreos/flannel/master/Documentation/kube-flannel.yml > kube-flannel.yaml
```

La configuración predeterminada que proporciona CoreOS utiliza el modo `vxlan` y la la arquitectura AMD64 en lugar de ARM. Para solucionar esto, se puede modificar manualmente el archivo de configuración descargado y reemplazar vxlan por host-gw y todas las instancias de amd64 por arm. Otra solución es usar `sed` para la sustitución de estos parámetros sin tener que hacerlo manualmente. Con el parámetro `s` realizamos la sustitución y con el `g` reemplaza todas las ocurrencias.

```bash
curl https://rawgit.com/coreos/flannel/master/Documentation/kube-flannel.yml | sed "s/amd64/arm/g" | sed "s/vxlan/host-gw/g" > kube-flannel.yaml
``` 
Después ejecutar el siguiente comando para crear dos objetos, un **ConfigMap usado para configurar Flannel y un DaemonSet que ejecuta el demonio Flannel real**. 
```bash
kubectl apply -f kube-flannel.yaml
``` 
Para ver que están corriendo los nuevos pods ejecutar:
```bash
kubectl get po --all-namespaces
```
Si se quiere inspeccionar su contenido, ejecutar:
```bash
kubectl describe --namespace=kube-system configmaps/kube-flannel-cfg
kubectl describe --namespace=kube-system daemonsets/kube-flannel-ds
```

## ¿Cómo configurar el panel de control de Kubernetes?

Si se quiere aprovechar el `panel de control o dashboard`, también conocida como Web UI o interfaz de usuario de Kubernetes, ejecutar<cite> [^8]:</cite>
[^8]: [Kubernetes Documentation. Web UI (Dashboard)](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.1.0/aio/deploy/recommended.yaml
``` 
Para acceder al panel de control usando el cli de `kubectl` se ejecuta:
```bash
kubectl proxy --accept-hosts='^*$'
```
Sólo se puede acceder a la interfaz de usuario desde la máquina donde se ejecuta el comando. Para verla desde el ordenador, es necesario crear un usuario usando la cuenta de servicio (Service Account) de Kubernetes y otorgarle permisos de administrador. Una vez creado el usuario con permisos, se iniciará sesión a través de un `Bearer Token` vinculado al usuario<cite>[^9].</cite> 
{{< hint danger >}}
**IMPORTANTE: Es necesario saber que garantizar privilegios de administrador a un usuario que se comunica sin SSL/TLS con el panel de control, es un riesgo de seguridad al ser vulnerable a ataques MITM.** 
{{< /hint >}}

La advertencia anterior no aplica en este caso porque se está desplegando en una red doméstica y no está expuesto al exterior.

[^9]: [Dashboard GitHub. Creating sample user](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md)

### Creación de ServiceAccount
La creación de la cuenta de servicio `admin-user` se realiza mediante el comando a continuación:
```bash
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
EOF
``` 
### Creación de ClusterRoleBinding
Se otorgan los privilegios de administrador a través del siguiente mandato:
```bash
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
EOF
```

El error de `There is nothing to display here` puede deberse a que se accede al dashboard con el rol admin-user sin el usuario necesario para acceder a los datos de kubernetes. La [solución encontrada](https://github.com/Azure/AKS/issues/1573) propone eliminar los privilegios antiguos y se asignar nuevos vinculados al usuario `clusterUser`.

```bash
kubectl delete clusterrolebinding kubernetes-dashboard
kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard --user=clusterUser
``` 

La creación del `Bearer Token` que nos permitirá iniciar sesión lo devolverá el resultado del siguiente comando:
```bash
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
``` 
Copiarlo en un sitio seguro y abrir una sesión ssh desde otro terminal con lo siguiente:
```bash
ssh -L8001:localhost:8001 <usuario>@<dirección_IP_master>
```  
Acceder desde un navegador a la dirección [http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/). Ahora pedirá un método de autenticación, escogemos token y pegamos el `Bearer Token` obtenido anteriormente.

### Eliminación de ServiceAccount y ClusterRoleBinding
Como se ha mencionado antes el riesgo de seguridad que conlleva acceder sin cifrado TLS/SSL, se recomienda eliminar la cuenta y sus privilegios después de la configuración. Se lleva a cabo mediante las siguientes órdenes:
```bash
kubectl -n kubernetes-dashboard delete serviceaccount admin-user
kubectl -n kubernetes-dashboard delete clusterrolebinding admin-user
``` 
### Eliminar proceso de kube-proxy

En caso obtener el error:
```
error: listen tcp 127.0.0.1:8001: bind: address already in use
```
La forma de eliminar el proceso es:
1. Obteniendo el PID del proceso.
```
ps aux | grep kubectl
```
2. Matando el proceso.
```
kill -9 <num-proceso>
```

### Error de proxy: context canceled

Un error común en la ejecución del proxy es `proxy_server.go:144] Error while proxying request: context canceled`. Se soluciona mejorando la conexión WiFi de las Raspberry Pis acercandolas al router y bajando la frecuencia con la que SSH revisa que el servidor está activo. Para ello, se añade al archivo `.ssh/config` los siguientes parámetros para el nodo master.

```
  ServerAliveInterval 120
  ServerAliveCountMax 30
```

### Despliegue del servidor de métricas

Se recomienda utilizar el servidor de métricas de kubernetes ([metric-server](https://github.com/kubernetes-sigs/metrics-server)) para realizar un autoescalado horizontal en función de la CPU y la memoria en uso de los nodos. También realiza automáticamente el ajuste de los recursos necesarios de los contenedores. Se instala la última `release` mediante el comando:
```bash
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
``` 
Para verificar que el servidor de métricas está corriendo ejecutar:
```bash
kubectl get deployment metrics-server -n kube-system
```
Para visualizar los datos de CPU y memoria de los nodos a través del CLI `kubectl top` ejecutar:
```bash
kubectl top nodes
```
Mientras que para visualizar los datos de CPU y memoria de los pods ejecutar:
```bash
kubectl top pods -A
``` 

### Errores en el servidor de métricas

En caso de obtener el error de `CrashLoopBackOff` y que se reinicie el pod cada poco tiempo, se recomienda añadir el flag `--kubelet-insecure-tls` a la configuración del archivo de metric-server. También se ha simplificado el argumento `kubelet-preferred-address-types=InternalIP`. Para que se muestre algo como esto:
```yaml
      containers:
      - args:
        - --cert-dir=/tmp
        - --secure-port=4443
        - --kubelet-insecure-tls
        - --kubelet-preferred-address-types=InternalIP
        - --kubelet-use-node-status-port
```
Para editarlo se puede utilizar:
```
KUBE_EDITOR=nano kubectl edit deployments.apps -n kube-system metrics-server
```

En caso de seguir obteniendo un error como este: `Error from server (ServiceUnavailable): the server is currently unable to handle the request (get nodes.metrics.k8s.io)`. Se puede conseguir más información del error con el comando:
```
kubectl get apiservice v1beta1.metrics.k8s.io -o yaml
```
El mensaje de error muestra lo siguiente:
```
'failing or missing response from https://10.103.31.80:443/apis/metrics.k8s.io/v1beta1:
      Get "https://10.103.31.80:443/apis/metrics.k8s.io/v1beta1": net/http: request
      canceled while waiting for connection (Client.Timeout exceeded while awaiting
      headers)'
    reason: FailedDiscoveryCheck
```
Indica que no tiene respuesta desde el nodo.

## Resetear clúster

Para resetear los cambios realizados y dejar una instalación limpia de Kubernetes en el clúster, ejecutar:

```
sudo kubeadm reset
```
