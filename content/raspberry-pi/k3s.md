---
title: Instalación de k3s y configuración del clúster
date: 2020-12-11
draft: false
weight: 23
---

{{< toc >}}

Para la instalación y acceso de la distribución más ligera de Kubernetes orientada a IoT y Raspberry Pi conocida como k3s, se toma como referencia un artículo de Opensurce<cite> [^1].</cite> 

[^1]: [Opensource, Run Kubernetes on a Raspberry Pi with k3s](https://opensource.com/article/20/3/kubernetes-raspberry-pi-k3s)

## Instalación en el nodo máster 

La instalación en el nodo máster se realiza con:
```
curl -sfL https://get.k3s.io | sh -
```
Se comprueba el servicio con:
```
sudo systemctl status k3s
```
Y la creación del clúster con:
```
sudo k3s kubectl get nodes
```
Se obtiene el token con:
```
sudo cat /var/lib/rancher/k3s/server/node-token
```

## Instalación de los nodos trabajadores

La instalación de los nodos trabajadores tiene este formato:
```
sudo curl -sfL https://get.k3s.io | K3S_URL=”https://<IP-nodo-master>:6443” K3S_TOKEN=token_obenido sh -
```
En este caso en concreto:
```
sudo curl -sfL https://get.k3s.io | K3S_URL="https://10.0.0.1:6443" K3S_TOKEN=K1043d62163a2887b0734bd719c7c1ba3724861824f90e91d81491643998713fb85::server:b7fe03dba3cd3ca4d5430365c2aa6641 sh -
```
Otra manera sería exportando el token a una variable de entorno:
```
export TOKEN_K3S=K1043d62163a2887b0734bd719c7c1ba3724861824f90e91d81491643998713fb85::server:b7fe03dba3cd3ca4d5430365c2aa6641
```
Y aplicando el comando:
```
sudo curl -sfL https://get.k3s.io | K3S_URL="https://10.0.0.1:6443" K3S_TOKEN=TOKEN_K3S sh -
```

## Acceso al clúster desde un ordenador

Desde el nodo máster se ejecuta:
```
sudo cat /etc/rancher/k3s/k3s.yaml
```
Se crea desde el ordenador la carpeta `.kube`:
```
mkdir ~/.kube
```
Se copia lo devuelto del nodo máster en el archivo .kube/config y cambiar la línea del servidor `server: https://127.0.0.1:6443` por:
```
server: https://master:6443
```
Al acceder en el navegador a la URL `https://master:6443` aparecerá un mensaje diciendo que no está autorizado.

Para instalar `kubectl` se precede con:

+ Distribuciones basadas en Arch a través de snap:
```
snap install kubectl --classic
```
+ Distribuciones basadas en Debian:
```
sudo apt update && sudo apt install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | \
sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt update && sudo apt install kubectl
```

Para ver los nodos en manjaro:
```
snap run kubectl get nodes
```

Para mostrar todo lo que trae preconfigurado:
```
sudo k3s kubectl -n kube-system get all
```
### Creación de un alias del comando sudo k3s kubectl

Se utiliza un alias para mejorar la eficiencia y acortar el tiempo de escritura de comandos.
```bash
alias k='sudo k3s kubectl'
```

A partir de ahora se puede utilizar `k` en vez de `sudo k3s kubectl`. 


## Desinsalar k3s

Para desinstalar k3s desde un nodo servidor se ejecuta<cite> [^2]:</cite>
[^2]: [Rancher, Uninstall](https://rancher.com/docs/k3s/latest/en/installation/uninstall/)

```
sudo sh /usr/local/bin/k3s-uninstall.sh
```
Desde los nodos trabajadores ejecutar:
```
sudo sh /usr/local/bin/k3s-agent-uninstall.sh
```

## Panel de control o Dashboard
### Instalación del Dashboard de Kubernetes

La instalación del panel de control o _dashboard_ se realiza siguiendo la documentación de Rancher<cite> [^3].</cite> Primero se crean las variables de entorno necesarias y el despliegue del dashboard con los siguientes comandos:
[^3]: [Rancher, Kubernetes Dashboard](https://rancher.com/docs/k3s/latest/en/installation/kube-dashboard/)

```bash
GITHUB_URL=https://github.com/kubernetes/dashboard/releases
VERSION_KUBE_DASHBOARD=$(curl -w '%{url_effective}' -I -L -s -S ${GITHUB_URL}/latest -o /dev/null | sed -e 's|.*/||')
sudo k3s kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/${VERSION_KUBE_DASHBOARD}/aio/deploy/recommended.yaml
``` 

Se continua creando el usuario `admin-user` con la autorización basada en roles (RBAC), con los siguientes ficheros:
+ dashboard.admin-user.yml
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
```
+ dashboard.admin-user-role.yml

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
```
Se despliegan la configuración del usuario:
```bash
sudo k3s kubectl create -f dashboard.admin-user.yml -f dashboard.admin-user-role.yml
```
Se obtiene el token de acceso del `admin-user` mediante el siguiente comando:
```bash
sudo k3s kubectl -n kubernetes-dashboard describe secret admin-user-token | grep ^token
```
Se accede localmente mediante proxy en la dirección [http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/).
```bash
sudo k3s kubectl proxy
```
### Actualización del Dashboard
Para la actualización del dashboard se utilizan los siguientes comandos:
```bash
sudo k3s kubectl delete ns kubernetes-dashboard
GITHUB_URL=https://github.com/kubernetes/dashboard/releases
VERSION_KUBE_DASHBOARD=$(curl -w '%{url_effective}' -I -L -s -S ${GITHUB_URL}/latest -o /dev/null | sed -e 's|.*/||')
sudo k3s kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/${VERSION_KUBE_DASHBOARD}/aio/deploy/recommended.yaml -f dashboard.admin-user.yml -f dashboard.admin-user-role.yml
```
### Eliminación del Dashboard

Para eliminar el despliegue y la configuración del `admin-user` se elimina directamete el espacio de nombres `kubernetes-dashboard`:
```bash
sudo k3s kubectl delete ns kubernetes-dashboard
```

## Instalación del dashboard de Traefik

Se edita el fichero del ConfigMap de traefik con:
```bash
kubectl -n kube-system edit cm traefik
```
Y se añade las dos líneas de api y dashboard:
```                    
    [api]                                     
      dashboard = true                        
```
Se reinicia los deployments mediante los comandos:
```
kubectl -n kube-system scale deploy traefik --replicas 0
kubectl -n kube-system scale deploy traefik --replicas 1
```
Se realiza una redirección de puertos mediante el comando:
```
kubectl -n kube-system port-forward deployment/traefik 8080
```


## Despliegue de una aplicación

### Despliegue
Se crea el despliegue con:
```
sudo k3s kubectl create deploy frontend --image=robinforgood/webapp
```
### Servicio
Se expone el servicio mediante el comando:
```
sudo k3s kubectl expose deploy frontend --port 80
```
### Ingress
Se crea un archivo llamado `frontend-ingress.yaml` con el siguiente contenido<cite> [^4]:</cite> 

[^4]: [Just me and Opensource, Rancher K3S Ingress Demo with Traefik](https://www.youtube.com/watch?v=12taKl5iCpA)

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: frontend
  annotations:
    ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: frontend
            port:
              number: 80
```
Para realizarlo a través de TLS mirar la [documentación oficial de kubernetes de ingress con TLS](https://kubernetes.io/docs/concepts/services-networking/ingress/#tls). 


### Ver logs de un despliegue

Para visualizar los resgistros de log de un deployment ejecutar:
```
sudo k3s kubectl logs frontend
```

### Despliegue del backend

#### Nodejs

Se crea el despliegue con:
```
sudo k3s kubectl create deploy backend --image=robinforgood/backend
```
Se expone el servicio mediante el comando:
```
sudo k3s kubectl expose deploy backend --port 8080:8080
```

## Despliegue de aplicaciones con Kubesail

Se crea una cuenta en [Kubesail](https://kubesail.com) asociada a una cuenta de GitHub mediante OAuth. Se conecta con el cluster mediante el siguiente comando:

```bash
sudo k3s kubectl create -f https://byoc.kubesail.com/robinforgood.yaml
```
Para ver los logs del pod que se usa como agente para conectar con kubesails se utiliza:
```
k logs kubesail-agent-6f68dc86bb-6d5h2 --namespace=kubesail-agent
```

El error que se muestra en el pod de kubesail con el siguiente log `error: Unable to resolve DNS! Falling back to CloudFlare DNS as backup! Please check your cluster for DNS capabiltiies! { "errMsg": "queryA ETIMEOUT api.kubesail.com", "code": "ETIMEOUT" }`, significa que no se puede resolver el DNS de api.kubesail.com. Para resolverlo se reinician los nodos o se elimina el namespace del agente mediante `k delete namespace kubesail-agent` y se vuelve a crear.

Se escoje la plantilla deseada de una aplicación y se despliega. Sin embargo, al desplegar la imagen oficial de postgres, se encuentran errores para conectarla con el ORM de la aplicación de Nodejs. 

## Recursos adicionales

1. [Rancher, Quick Start](https://rancher.com/docs/os/v1.x/en/quick-start-guide/)

2. [Rancher, Raspberry Pi](https://rancher.com/docs/os/v1.x/en/installation/server/raspberry-pi/)

3. [Self-Hosting Your Homelab Services with SSL -- Let's Encrypt, MetalLB, Traefik, Rancher, Kubernetes](https://www.youtube.com/watch?v=pAM2GBCDGTo)

4. [Telepresence](https://www.telepresence.io/)

5. [K3sup](https://github.com/alexellis/k3sup)
6. [I - Pi & Kubernetes with k3s, k3sup, arkade and OpenFaaS](https://www.youtube.com/watch?v=ZiR3QEfBivk)