---
title: Configuración del servidor DHCP
date: 2020-12-11
draft: false
weight: 0
---

{{< toc >}}

Primero se ha configurado la interfaz wlan0 durante la instalación de HypriotOS para las conexiones con el exterior del cluster. El siguiente paso es configurar la subred interna. Se comienza configurando en nodo master.  

## ¿Cómo se fija la dirección IP estática para la subred interna del clúster?

Se crea el fichero `/etc/network/interfaces.d/eth0` con este contenido:

```
allow-hotplug eth0
iface eth0 inet static
    address 10.0.0.1
    netmask 255.255.255.0
    broadcast 10.0.0.255
    gateway 10.0.0.1
``` 

Después de guardar el fichero es necesario reiniciar el sistema con `sudo reboot` para que se apliquen los cambios. El siguiente paso es configurar el servidor DHCP en el nodo master para que asigne las direcciones IP a los nodos trabajadores.

## ¿Cómo se configura DHCP en el nodo master?

Lo primero es instalar DHCP en el nodo máster con el comando:
```bash
sudo apt install isc-dhcp-server
```
Se continúa con la configuración del servidor DHCP editando el fichero `/etc/dhcp/dhcpd.conf`<cite> [^1]:</cite>
[^1]: [Debian Wiki, DHCP_Server](https://wiki.debian.org/DHCP_Server)
```
# Configura un nombre de dominio, puede ser cualquiera
option domain-name "cluster.home";
# Se puede usar los DNS públicos autoritarios de Cloudflare (1.1.1.1 y 1.0.0.1), OpenDNS (208.67.222.222 y 208.67.220.220), Google (8.8.8.8 y 8.8.4.4) o del ISP. 
option domain-name-servers 1.1.1.1, 1.0.0.1;

subnet 10.0.0.1 netmask 255.255.255.0 {
    range 10.0.0.1 10.0.0.10;
    option subnet-mask 255.255.255.0;
    option broadcast-address 10.0.0.255;
    option routers 10.0.0.1;
}

default-lease-time 600;
max-lease-time 7200;
authoritative;
```
Se añade la interfaz `eth0` a INTERFACES en el archivo `/etc/default/isc-dhcp-server`. Para ello se edita el fichero:
```bash
cd /etc
cd defaults
sudo nano isc-dhcp-server
```
Se descomenta la interfaz de IPv4:
```
INTERFACESv4="eth0"
#INTERFACESv6="eth0"
```
Y se reinicia el servicio `isc-dhcp-server` y se reinicia la máquina:
```bash
sudo systemctl restart isc-dhcp-server
sudo reboot
```
Después del reinicio, comprobar el estado del servicio:
```
sudo systemctl status isc-dhcp-server
```
En caso de que haya algún error en el servicio al reiniciar el sistema, parar el servicio con `sudo systemctl stop isc-dhcp-server` asegurarse de que está comentada la línea de `#INTERFACESv6="eth0"` y reiniciar el sistema. 

Una vez reiniciado, se conectan las máquinas mediante un cable ethernet y se comprueba que el nodo worker tiene asignada la dirección IP `10.0.0.2` con el mandato:
```bash
ip a
sudo ip addr show
```
